
//Title:        Shopping Cart Web Application
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       
//Company:      QA Training
//Description:  Classes for use with an online toy shop

package utils;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.io.*;

public class ElapsedTimeFinder extends TagSupport {

private long startTime;

  public ElapsedTimeFinder() {
  }

  public void setStartTime(long newStartTime){
    System.out.println("ElapsedTimeFinder: entered setStartTime(long) with time of " + newStartTime);
    startTime = newStartTime;
  }

  public int doStartTag() throws JspException {
    System.out.println("ElapsedTimeFinder: entered doStartTag()");
    try{
     long  now = new Date().getTime();
     now -= startTime;
     pageContext.getOut().print(now + " milliseconds");
    }catch(IOException io){
      System.err.println(io);
    }
     return Tag.SKIP_BODY;
  }

  public int doEndTag() throws JspException{
    System.out.println("ElapsedTimeFinder: entered doEndTag()");
    return Tag.EVAL_PAGE;
  }
}