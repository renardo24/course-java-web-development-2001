
//Title:        Shopping Cart Web Application
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       
//Company:      QA Training
//Description:  Classes for use with an online toy shop

package utils;

import java.util.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class Iterate extends BodyTagSupport{

  private Object item;
  private Enumeration enum;

  public Iterate() {
  }

  public Object getItem() {
    System.out.println("Iterate:  getItem() ");
    return item;
  }

  public void setEnumeration(Enumeration enumeration) {
      System.out.println("Iterate: setEnumeration() ");
    enum =  enumeration;
  }

  public int doStartTag(){
      System.out.println("Iterate: doStartTag() ");
    if(enum.hasMoreElements())
    {
       item = enum.nextElement();
       return BodyTag.EVAL_BODY_TAG;
    }else return Tag.SKIP_BODY;
  }

  public int doAfterBody(){
      System.out.println("Iterate:  doAfterBody()");
    if(enum.hasMoreElements())
    {
       item = enum.nextElement();
       return BodyTag.EVAL_BODY_TAG;
    }else
    {
      try{
        bodyContent.writeOut(bodyContent.getEnclosingWriter());
      }catch(IOException io){
          io.printStackTrace();
      }
      return Tag.SKIP_BODY;
    }
  }
} 