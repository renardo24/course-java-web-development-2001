
//Title:        Shopping Cart Web Application
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       
//Company:      QA Training
//Description:  Classes for use with an online toy shop

package cart;

import javax.servlet.jsp.*;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.*;
import java.util.*;
import java.io.*;

public class ShowCartItems extends BodyTagSupport{

  private Enumeration enum;

  public ShowCartItems() {
  }

  public void setCartHoldingItems(ShoppingCart aCart) {
    System.out.println("ShowCartItems: in setCartHoldingItems()");
    enum = aCart.getCartItems();
  }

  public int doStartTag() throws JspException{
    System.out.println("ShowCartItems: in doStartTag()");
    return BodyTag.EVAL_BODY_TAG;
  }

  public void doInitBody(){
     System.out.println("ShowCartItems: in doInitBody()");
     if(enum.hasMoreElements())
     {
        setCartItemData();
     }
  }

  private void setCartItemData(){
      Cartable cartItem = (Cartable) enum.nextElement();
      pageContext.setAttribute("code",cartItem.getCode());
      pageContext.setAttribute("description",cartItem.getDescription());
      pageContext.setAttribute("quantity",new Integer(cartItem.getQuantity()));
      pageContext.setAttribute("cost",new Double(cartItem.getCost()));
  }

  public int doAfterBody() throws JspException{
    System.out.println("ShowCartItems: in doAfterBody()");
    if(enum.hasMoreElements())
    {
       setCartItemData();
       return BodyTag.EVAL_BODY_TAG;
    }
    else{
      try{
        bodyContent.writeOut(bodyContent.getEnclosingWriter());
      }catch(IOException io){
          io.printStackTrace();
      }
      return Tag.SKIP_BODY;
    }
  }


} 