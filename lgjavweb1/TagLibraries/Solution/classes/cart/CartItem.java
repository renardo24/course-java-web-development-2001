package cart;

public class CartItem implements Cartable  {

private String code;
private String description;
private int quantity;
private double cost;

  public CartItem() {
  }

  public CartItem(String aCode, String aDescription, int aQuantity, double aCost) {
    code = aCode;
    description = aDescription;
    quantity = aQuantity;
    cost = aCost;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public int getQuantity() {
    return quantity;
  }

  public double getCost() {
    return cost;
  }

  public void setCode(String aCode) {
    code = aCode;
  }

  public void setDescription(String aDescription) {
    description = aDescription;
  }

  public void setQuantity(int aQuantity) {
    quantity = aQuantity;
  }

  public void setCost(double aCost) {
    cost = aCost;
  }




}