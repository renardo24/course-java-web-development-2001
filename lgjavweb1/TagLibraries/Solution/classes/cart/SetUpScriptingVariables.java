package cart;


import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.util.*;

public class SetUpScriptingVariables extends TagSupport {
  private String scriptVariableName;

  public SetUpScriptingVariables() {
  }

  public String getId() {
    return scriptVariableName;
  }

  // example of setting up scripting variable to be known by name passed in as parameter
  // from now on the ShoppingCart can be referred to as <name>
  public void setId(String name){
    System.out.println("SetUpScriptingVariables: setId(String) setting scripting variable named  " + name);
    ShoppingCart cart = (ShoppingCart) pageContext.getSession().getValue("cart");
    if(cart == null){
      System.out.println("Could not find cart under name cart");
      return;
    }
    pageContext.setAttribute(name,cart);
    scriptVariableName = name;
  }
} 