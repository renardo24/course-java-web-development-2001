
set WARFILE=tagLib.war
set EARFILE=tagLib.ear

set CONTENT=tld/qatags.tld:html/AddToCart.html:jsp/showCart.jsp

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive -classpath classes . -contentFiles %CONTENT% web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% tagLib %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
