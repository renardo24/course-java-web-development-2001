package cart;

public interface Cartable {

String getCode();
String getDescription();
int getQuantity();
double getCost();

void setCode(String aCode);
void setDescription(String aDescription);
void setQuantity(int aQuantity);
void setCost(double aCost);

} 