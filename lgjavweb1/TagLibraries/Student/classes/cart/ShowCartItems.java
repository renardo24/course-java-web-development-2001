package cart;

import javax.servlet.jsp.*;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.*;
import java.util.*;
import java.io.*;

public class ShowCartItems extends BodyTagSupport{

  private Enumeration enum = null ;
  private ShoppingCart cartHoldingItems = null ;

  public int doStartTag() throws JspException{
    return BodyTag.EVAL_BODY_TAG;
  }

  public void setCartHoldingItems(ShoppingCart aCart) {
    System.out.println("ShowCartItems: in setCartHoldingItems()");
    enum = aCart.getCartItems();
  }


  public void doInitBody(){
  // QA ToDo:
  // Recover the next Cartable element from the enumeration,
  // extract the values for the properties
  // code, description, quantity and cost
  // and put these values into the pageContext object
  // using the setAttribute command.
    if ( enum != null )
    {
      ShoppingCart element = null ;
      if ( enum.hasMoreElements() )
      {
        setCartItemData() ;
      }
    }
  }

  private void setCartItemData(){
      Cartable cartItem = (Cartable) enum.nextElement() ;
      pageContext.setAttribute( "code", cartItem.getCode() ) ;
      pageContext.setAttribute( "description", cartItem.getDescription() ) ;
      pageContext.setAttribute( "quantity", new Integer(cartItem.getQuantity() ) ) ;
      pageContext.setAttribute( "cost", new Double(cartItem.getCost() ) ) ;
  }

  public int doAfterBody() throws JspException{
  // QA ToDo:
  // If the enumeration holds more Cartable items
  // recover the next Cartable element from the enumeration,
  // extract the values for the properties
  // code, description, quantity and cost
  // and put these values into the pageContext object
  // using the setAttribute command.
  // Return BodyTag.EVAL_BODY_TAG.
    if ( enum != null )
    {
      ShoppingCart element = null ;
      if ( enum.hasMoreElements() )
      {
        setCartItemData() ;
      }
    return BodyTag.EVAL_BODY_TAG ;
    }
    else
    {
  // QA ToDo:
  // Alternatively, if there are no more Cartable items
  // write out the bodyContent to the enclosing JspWriter.
      try
      {
        bodyContent.writeOut( bodyContent.getEnclosingWriter() ) ;
      }
      catch( IOException io )
      {
        io.printStackTrace() ;
      }
      return Tag.SKIP_BODY;
    }
  }
}
