package utils;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.io.*;

// QA ToDo:
// You need to implement Tag (or extend from TagSupport)
public class ElapsedTimeFinder extends TagSupport {

  // QA ToDo:
  // provide a JavaBean property of type long called startTime
  private long startTime = 0 ;

  public ElapsedTimeFinder() {
  }

  public long getStartTime()
  {
    return startTime ;
  }
  public void setStartTime( long st )
  {
    startTime = st ;
  }


//QA ToDo:
// Write the doStartTag() method to calculate the elapsed time
// output the result to the JSP
// and return the appropriate code - a constant from the Tag interface
  public int doStartTag()
  {
    try
    {
      long now = System.currentTimeMillis() ;
      JspWriter out = pageContext.getOut() ;
      long difference = now - startTime ;
      out.print( "Elapsed time difference: " + difference ) ;
    }
    catch ( IOException ioe ) {}
    return Tag.SKIP_BODY ;
  }

}