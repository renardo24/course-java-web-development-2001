<html>
<HEAD>
<TITLE>Cart Contents</TITLE>
<%@ page session="true"%>
<%@ page import="java.util.*" %>
<%@ page import="cart.*" %>
<%@ taglib uri="http://www.lib_oli.com" prefix="qa" %>
<jsp:useBean id='cart' scope='session' class='cart.ShoppingCart' />
</HEAD>

<BODY BGCOLOR="black" TEXT= "white">

<FONT SIZE=4>
<UL>
<LI>
Cart belongs to : <jsp:getProperty name="cart" property="cartOwner" /></li>
<BR>
<BR>
<li>Session Duration : <qa:GetElapsedTime startTime="<%= session.getCreationTime() %>" />
</li>
</UL>
</FONT>

<TABLE ALIGN="center" WIDTH="100", CELLSPACING="4" CELLPADDING="4">

<qa:ShowCartItems cartHoldingItems="<%= cart %>">
	<TR>
    <TD><%= code %></TD>
    <TD><%= description %></TD>
    <TD><%= quantity %></TD>
    <TD><%= cost %></TD>
  </TR>
</qa:ShowCartItems>

</TABLE>

<BR>
<HR>
<BR>


</BODY>
</HTML>
