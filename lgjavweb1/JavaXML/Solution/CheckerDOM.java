
import java.io.File;

import org.w3c.dom.*;
import org.xml.sax.*;
import org.apache.xerces.parsers.*;

/**
 * Sample solution for DOM based XML reader exercise
 */

public class CheckerDOM extends CheckerBase {

  /**
   * recursively check the contents of the given directory
   *
   * @param dirElement the DOM element representing the directory
   * @param curDir a File for the directory
   */

  void checkDirectory(Element dirElement, File curDir) {

	  // check to see if the directory has changed
	  checkOne(curDir, null, dirElement.getAttribute("modtime"));

	  // now check each of the directory element's children
	  for(Node n = dirElement.getFirstChild();
	      n != null;
	      n = n.getNextSibling()) {
      // only look at element nodes
	    if(n.getNodeType() == Node.ELEMENT_NODE) {
		    // make a File for the entity represented by the element
		    Element entry = (Element)n;
		    File curFile = new File(curDir, entry.getAttribute("name"));

		    if(entry.getTagName().equals("directory")) {
		      // recursively process the directory's contents
		      checkDirectory(entry, curFile);
        }
		    else {
		      // check the file
		      checkOne(curFile,
                   entry.getAttribute("size"),
                   entry.getAttribute("modtime"));
		    }
	    }
    }
  }

  /**
   * parse the supplied XML file and check whether the files and
   * directories that it specifies have changed
   *
   * @param XMLFile the name of the XML file to read
   */
  void checkAll(String XMLFile) throws Exception {
	  // get a DOMParser
	  DOMParser p = new DOMParser();
	  try {
	    // turn on validation -- this is important because we want
	    // to be sure that all the attributes have been specified
	    p.setFeature("http://xml.org/sax/features/validation", true);
	    // set the error handler -- this is also important because
	    // otherwise we won't get any validation error messages
	    p.setErrorHandler(this);
	    // parse the supplied file
	    p.parse(XMLFile);
	  }
	  catch (SAXException e) {
	      System.err.println(e.getMessage());
	      return;
	  }
	  // extract the DOM document
	  Document doc = p.getDocument();
	  // get the 'root' element
	  Element rootElement = doc.getDocumentElement();
	  // get the topmost 'directory' element
	  Element topDirElement = (Element)rootElement.getElementsByTagName("directory").item(0);
	  // create a File for the topmost directory
	  File topDir = null;
	  if(!(rootElement.getAttribute("prefix").equals("")))
	    topDir = new File(rootElement.getAttribute("prefix"),
			                  topDirElement.getAttribute("name"));
	  else
	    topDir = new File(".", topDirElement.getAttribute("name"));
	  // check the directory and its contents
	  checkDirectory(topDirElement, topDir);
  }

  /**
   * Simple command line test harness
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) throws Exception {
	  // check using the file supplied on the command line
	  if(args.length > 0)
	    new CheckerDOM().checkAll(args[0]);
  }
}

