
import java.io.*;
import org.w3c.dom.*;
import org.apache.xerces.dom.*;
import org.apache.xml.serialize.*;

/**
 * write output from file system walker as XML using DOM
 */

class FileSystemWalkerDOM implements FileSystemWalkerHandler {

  PrintStream out;       // the output stream
  Document doc;          // the whole DOM document
  Element curDirElement; // the DOM element for the current directory

  FileSystemWalkerDOM(PrintStream out) {
    this.out = out;
  }

  /**
   * called at start of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsStart(File rootDir) {
	  DOMImplementation domImpl = DOMImplementationImpl.getDOMImplementation();
	  DocumentType docType = domImpl.createDocumentType("root", null, "fs.dtd");
	  doc = domImpl.createDocument(null, "root", docType);
	  curDirElement = doc.getDocumentElement();
	  String prefixAttr = rootDir.getParent();
	  if(prefixAttr == null)
	    prefixAttr = "";
	  curDirElement.setAttribute("prefix", prefixAttr);
  }

  /**
   * called at end of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsEnd(File rootDir) {
	  OutputFormat of = new OutputFormat(doc, "UTF-8", true);
	  XMLSerializer s = new XMLSerializer(out, of);
	  try {
	    s.serialize(doc);
	  }
	  catch (IOException e) {
	    System.err.println(e);
	  }
  }

  /**
   * called at start of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirStart(File directory, int depth) {
	  Element newDir = doc.createElement("directory");
	  curDirElement.appendChild(newDir);
	  curDirElement = newDir;
	  curDirElement.setAttribute("name", directory.getName());
	  curDirElement.setAttribute("modtime", "" + directory.lastModified());
  }

  /**
   * called at end of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirEnd(File directory, int depth) {
  	if(depth > 0)
	    curDirElement = (Element)curDirElement.getParentNode();
  }

  /**
   * called when processing a file
   *
   * @param file the file being processed
   * @param depth the directory nesting
   */
  public void file(File file, int depth) {
	  Element newFile = doc.createElement("file");
	  newFile.setAttribute("name", file.getName());
	  newFile.setAttribute("size", "" + file.length());
	  newFile.setAttribute("modtime", "" + file.lastModified());
	  curDirElement.appendChild(newFile);
  }

  /**
   * command line utility to output file tree information as XML
   */
  public static void main(String[] args) throws Exception {
    PrintStream out = System.out;
    if(args.length > 1)
      out = new PrintStream(new FileOutputStream(args[1]));
	  if(args.length > 0)
	    new FileSystemWalker(new FileSystemWalkerDOM(out)).walk(args[0]);
    else
      System.err.println("usage: FileSystemWalkerDOM rootdir [outfile]");
  }
}
