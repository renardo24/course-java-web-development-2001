package servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class WelcomeServlet extends HttpServlet {

  //Initialize global variables
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  //Process the HTTP Post request
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = new PrintWriter (response.getOutputStream());
    out.println("<html>");
    out.println("<head><title>WelcomeServlet</title></head>");
    out.println("<body bgcolor=#FF66AA>");
    out.println("<BR><BR><H1>WELCOME SERVLET</H1>");
    out.println("<BR><BR>Username = " + request.getRemoteUser());
    out.println("<BR><BR>Principal = " + request.getUserPrincipal());
    out.println("<BR><BR>Authentication Approach = " + request.getAuthType());
    out.println("<BR><BR>Is the request secure? = " + request.isSecure() );
    out.println("<BR><BR>Is the caller in role 'admin' ? = " + request.isUserInRole("admin") );
    out.println("<BR><BR>Is the caller in role 'customer' ? = " + request.isUserInRole("customer") );
    out.println("<BR><BR><HR>");
    out.println("<BR><BR><A HREF='SecurityServlet'>Go to SecurityServlet</A>");
    out.println("<BR><BR><A HREF='HTTPSServlet'>Go to HTTPSServlet</A>");    
    out.println("</body></html>");
    out.close();
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request,response);
  }  

  //Get Servlet information
  public String getServletInfo() {
    return "WelcomeServlet Information";
  }
}
 