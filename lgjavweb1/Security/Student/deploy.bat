@echo off
@REM -- Usage --
@if not "%1"=="" goto begin
echo Usage: deploy [myApp.EAR]
@goto end

:begin
@echo on
c:\j2sdkee1.2.1\bin\deploytool -deploy %1 localhost
@echo off

:end