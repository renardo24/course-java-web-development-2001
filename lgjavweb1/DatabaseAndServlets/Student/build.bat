
set WARFILE=databaseAndServlets.war
set EARFILE=databaseAndServlets.ear

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive -classpath . -classFiles SetupDBServlet.class:DisplayOrdersServlet.class . -contentFiles Login.html web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% DatabaseAndServlets %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
