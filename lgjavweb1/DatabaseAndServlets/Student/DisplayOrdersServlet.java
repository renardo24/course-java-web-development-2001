import javax.servlet.http.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import javax.servlet.*;
import java.io.*;

// exercise two step one
// have the DisplayOrdersServlet class subclass HttpServlet and
// also implement the single thread model

public class DisplayOrdersServlet extends HttpServlet implements SingleThreadModel
{

// exercise two step two
// declare private variables to hold references to:
// a JDBC Connection object
// a JDBC Statement object
// a JDBC ResultSet object; and
// a PrintWriter object called out
private Connection con = null ;
private Statement stat = null ;
private ResultSet rs = null ;
private PrintWriter out = null ;

  public void init()
  {

// exercise two step three
// use JNDI to obtain a connection to the database "jdbc/ServletDB"
try
{
  Context ctx = new InitialContext() ;
  DataSource ds = (DataSource) ctx.lookup( "jdbc/ServletDB" ) ;
  con = ds.getConnection() ;
}
catch ( NamingException ne )
{
  out.println( ne.getMessage() ) ;
}
catch ( SQLException sqle )
{
  out.println( sqle.getMessage() ) ;
}

  }

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step four
// have doGet simply pass through the request to doPost
doPost( req, rsp ) ;

  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step five
// set the response's content type
// retrieve the writer for the response object and store a reference to it in
// the out PrintWriter variable
// if no id parameter is present, call the doError method passing the appropriate parameters
// otherwise, call doHeader, mainTitle, doOrderTable and doFooter in that order
rsp.setContentType( "text/html" );
try
{
  out = rsp.getWriter() ;
}
catch ( IOException ioe ) {}

if ( req.getParameter( "id" ) == null )
  doError( req, rsp ) ;
else
{
  doHeader( req, rsp ) ;
  mainTitle( req, rsp ) ;
  doOrderTable( req, rsp ) ;
  doFooter( req, rsp ) ;
}


  }

  public void doError(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step six
// build a full response which reports the problem to the user
// provide them with a hyperlink back to the login page to enable
// them to fill out the form and re-submit it
out.println( "<html><head><title>Error</title></head>" ) ;
out.println( "<body><h1>No ID was submitted</h1>" ) ;
out.println( "<p><a href=\"/Login.htm\">Back To Login</a></p>/body></html>" ) ;

  }

  public void doHeader(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step seven
// Build up the response page from the opening <HTML> tag to the close of the </HEAD> tag
// Include a title which describes the customer by name or id as appropriate
out.println( "<html><head><title>" + req.getParameter( "id" ) + "</title></head>" ) ;

  }

  public void mainTitle(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step eight
// open the HTML body tag and provide a Header which notes which customer we are
// displaying order history for. Break up the page with the inclusion of a horizontal line
out.println( "<body><h1>Customer ID: " + req.getParameter( "id" ) +"</h1>" ) ;

  }

  public void getData(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step nine
// build the SQL to be sent to the database
// append a where clause and search by customer id (custid)
// if one is provided, else add a where clause which searches by forename and surname
// if you are unfamiliar with SQL, a solution to this step is in the file sql.txt
String s =  null ;
if ( req.getParameter( "id" ) != null )
  s = "SELECT * FROM Customer where custid = " +  req.getParameter( "id" ) ;
else if ( req.getParameter( "forename" ) != null )
  s = "SELECT * FROM Customer where forename = " +  req.getParameter( "forename" ) ;
else if ( req.getParameter( "surname" ) != null )
  s = "SELECT * FROM Customer where surname = " +  req.getParameter( "surname" ) ;

// exercise two step ten
// create a Statement for use with the connection we have acquired
// Use the statement object to execute the query and store the returned ResultSet reference
// in the ResultSet variable declared in step two of the exercise. Add appropriate exception handling.
try
{
  stat = con.createStatement() ;
  rs = stat.executeQuery( s ) ;
}
catch (SQLException sqle )
{
  out.println( sqle.getMessage() ) ;
}


  }

  public void doOrderTable(HttpServletRequest req, HttpServletResponse rsp)
  {
    getData(req, rsp);
    try
    {
// exercise two step eleven
// add code which builds an HTML table of results based on the ResultSet returned in getData()
      out.println( "<table width=\"100%\"> border=\"1\"" ) ;
      out.println( "<tr><td>Customer ID</td><td>Forename</td><td>MiddleInitial</td><td>Surname</td><td>ContactNumber</td><td>HouseNumber</td><td>PostCode</td></tr>" ) ;

      while ( rs.next() )
      {
        out.println( "<tr>" ) ;
        for ( int i = 1; i <= 7; i++ )
          out.println( "<td>" + rs.getString( i ) + "</td>" ) ;
        out.println( "</tr>" ) ;
      }

      out.println( "</table>" ) ;


    }
    catch(Exception e)
    {
      System.out.println("Data Access Error: " + e);
    }
  }

  public void doFooter(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step twelve
// complete the page, ensuring that no tags are left unclosed.
out.println("</body>") ;
out.println("</html>") ;

  }

}
