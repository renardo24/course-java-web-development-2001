import javax.servlet.http.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import javax.servlet.*;
import java.io.*;


public class DisplayOrdersServlet extends HttpServlet implements SingleThreadModel
{
  private Connection conn;

  private Statement stmt;

  private ResultSet rs;

  private PrintWriter out;


  public void init()
  {
    try {
      // get the initial context
      Context ic = new InitialContext();
      // now get a DataSource for the desired database
      DataSource ds = (DataSource)ic.lookup("jdbc/ServletDB");
      // now get a connection to that database
      conn = ds.getConnection();
    }
    catch(Exception e) {
      System.out.println("Error connecting to database: " + e);
    }
  }

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
    rsp.setContentType("text/html");
    try {
      out = rsp.getWriter();
    }
    catch(Exception e) {
      System.out.println("Error acquiring Writer: " + e);
    }

    if(req.getParameter("id")==null)
    {
      doError(req, rsp);
    }
    else
    {
      doHeader(req, rsp);
      mainTitle(req, rsp);
      doOrderTable(req, rsp);
      doFooter(req, rsp);
    }
  }

  public void doHeader(HttpServletRequest req, HttpServletResponse rsp)
  {
    out.println("<HTML>");
    out.println("<HEAD>");
    out.println("<TITLE>");
    out.println("Order list for customer " + req.getParameter("surname") + ", " + req.getParameter("forename") + req.getParameter("id"));
    out.println("</TITLE>");
    out.println("</HEAD>");
  }

  public void mainTitle(HttpServletRequest req, HttpServletResponse rsp)
  {
    out.println("<BODY>");
    out.println("<H1>");
    out.println("Order list for customer " + req.getParameter("surname") + ", " + req.getParameter("forename") + req.getParameter("id"));
    out.println("</H1>");
    out.println("<HR>");
  }

  public void doOrderTable(HttpServletRequest req, HttpServletResponse rsp)
  {
    getData(req, rsp);
    try
    {
      // Table title
      out.println("<TABLE BORDER=1><TR><TD><B>Product Ordered</B></TD>" +
                  "<TD><B>Quantity</B></TD><TD><B>Unit Price</B></TD>" +
                  "<TD><B>Total Order Cost</B></TD></TR>");
      // Returned data
      while(rs.next())
      {
        out.println("<TR>");
        for(int i=1;i<=4;i++)
        {
          out.println("<TD>" + rs.getString(i) + "</TD>");
        }
        out.println("</TR>");
      }
      out.println("</TABLE>");

    }
    catch(Exception e)
    {
      out.println("Data Access Error: " + e);
    }
  }

  public void doFooter(HttpServletRequest req, HttpServletResponse rsp)
  {
    out.println("</BODY>");
    out.println("</HTML>");
  }


  public void getData(HttpServletRequest req, HttpServletResponse rsp)
  {
    String sql = "SELECT  Product.Description, Orders.Quantity, Orders.UnitPrice, " +
        " (Orders.UnitPrice * Orders.Quantity) AS \"Total Cost\" " +
        " FROM Customer INNER JOIN " +
        " Orders ON Customer.custid = Orders.custid INNER JOIN " +
        " Product ON Orders.productid = Product.productid ";
    if("".equals(req.getParameter("id")))
    {
        sql += " WHERE forename = '" + req.getParameter("forename") + "'";
        sql += " AND surname = '" + req.getParameter("surname") + "'";
    }
    else
    {
        sql += " WHERE Customer.custid = " + req.getParameter("id");
    }
    try
    {
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);
    }
    catch(Exception e)
    {
      out.println("Data Access Error: " + e);
    }
  }

  public void doError(HttpServletRequest req, HttpServletResponse rsp)
  {
    out.println("<HTML>");
    out.println("<HEAD>");
    out.println("<TITLE>");
    out.println("Error");
    out.println("</TITLE>");
    out.println("</HEAD>");
    out.println("<BODY>");
    out.println("<H1>Please complete the form on the ");
    out.println("<A href = \"Login.html\">Login page</A>");
    out.println(" before navigating to this page.</H1>");
    out.println("</BODY>");
    out.println("</HTML>");
  }

}
