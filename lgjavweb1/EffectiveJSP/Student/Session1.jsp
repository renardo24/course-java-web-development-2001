
<%@ page import="java.util.*" %>

<html>
  <head>
    <title>Session 1</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>
  <h2><i>
<%
	if( session.isNew() )
	{
	  // it's a new session
%>

Hello! You are: <%= request.getRemoteHost() %>

<%
	}
	else
	{
%>

You (i.e. <%= request.getRemoteHost() %>) already have a session!

<%
	}
%>

  </i></h2>
  </body>
</html>
