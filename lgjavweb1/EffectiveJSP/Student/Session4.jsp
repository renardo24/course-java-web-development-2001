
<%@ page import="java.util.*" %>

<html>
  <head>
    <title>Session 1</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>

<%!
static class VisitCounter
{
	int count ;
	VisitCounter() { count = 0 ; }
	VisitCounter( int n ) { count = n ; }
	int getCount() { return count ; }
	void increment() { ++count ; }
}

%>

<%
	VisitCounter visitCounter = null;
%>

<%
	String s = request.getParameter( "logout" ) ;
	if ( ( s != null ) && ( s.equals( "Logout" ) ) )
	{
		session.invalidate() ;
%>
<form method="get" action="Session4">
<input type="submit" value="Login">
</form>
  <h2><i>
<%
	}
	else 
	{

		try 
		{
			visitCounter = (VisitCounter)session.getAttribute("visits") ;
		}
		catch (ClassCastException e) {}

		if(visitCounter == null) 
		{
			visitCounter = new VisitCounter() ;
			session.setAttribute("visits", visitCounter);
		}
		visitCounter.increment() ;
		
	
		if( session.isNew() )
		{
		  // it's a new session
		  Cookie[] cookies = request.getCookies();
		  for ( int i = 0; i < cookies.length; i++ )
		  {
		  	if ( cookies[i].getName().equalsIgnoreCase( "theCookie" ) )
		  	{
		        /* yes, create the visit counter initialised with the supplied value */
        		visitCounter = new VisitCounter( Integer.parseInt( cookies[i].getValue() ) ) ;
        		/* save the visit counter in the session */
        		session.setAttribute("visits", visitCounter);
        		break;
      			}
    		}

%>

Hello! You are: <%= request.getRemoteHost() %>

<%
		}
		else
		{
%>

<p>You (i.e. <%= request.getRemoteHost() %>) already have a session!</p>
<p>Visits so far this session: <%= visitCounter.getCount() %> </p>

<%
		}
%>

  </i></h2>


<form method="get" action="<%=response.encodeURL("Session4")%>">
<p><input type="submit" value="Reload"></p>
<p><input type="submit" value="Logout" name="logout"></p>
</form>
<%
		/* save the current value of the visit counter in a cookie */
		response.addCookie( new Cookie( "theCookie", "" + visitCounter.getCount() ) ) ;
   	}
%>
  </body>
</html>
