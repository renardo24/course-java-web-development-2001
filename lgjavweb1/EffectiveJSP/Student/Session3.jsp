
<%@ page import="java.util.*" %>

<html>
  <head>
    <title>Session 1</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>

<%!
static class VisitCounter
{
	int count = 0 ;
	int getCount() { return count ; }
	void increment() { ++count ; }
}

%>

<%
	String s = request.getParameter( "logout" ) ;
	if ( ( s != null ) && ( s.equals( "Logout" ) ) )
	{
		session.invalidate() ;
%>
<form method="get" action="Session3">
<input type="submit" value="Login">
</form>
  <h2><i>
<%
	}
	else 
	{

		VisitCounter visitCounter = null;
		try 
		{
			visitCounter = (VisitCounter)session.getAttribute("visits") ;
		}
		catch (ClassCastException e) {}

		if(visitCounter == null) 
		{
			visitCounter = new VisitCounter() ;
			session.setAttribute("visits", visitCounter);
		}
		visitCounter.increment() ;
		
	
		if( session.isNew() )
		{
		  // it's a new session
%>

Hello! You are: <%= request.getRemoteHost() %>

<%
		}
		else
		{
%>

<p>You (i.e. <%= request.getRemoteHost() %>) already have a session!</p>
<p>Visits so far this session: <%= visitCounter.getCount() %> </p>

<%
		}
   	}
%>

  </i></h2>


<form method="get" action="<%=response.encodeURL("Session3")%>">
<p><input type="submit" value="Reload"></p>
<p><input type="submit" value="Logout" name="logout"></p>
</form>
  </body>
</html>
