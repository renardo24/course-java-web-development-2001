
<%@ page import="java.util.*" %>

<html>
  <head>
    <title>Session 2</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>
<%
/* check if the logout parameter has a value of "Logout" */
String logout = request.getParameter("logout");
if(logout != null && logout.equals("Logout")) {
   /* yes, they have logged out */
   session.invalidate();
%>
Goodbye, please call again soon.
<hr>
<form method="get" action="Session2">
  <input type="submit" value="Login">
</form>
<%
  /* end logging out */
}
else {
  /* not logging out */
  if(session.isNew()) {
    /* it's a new session */
%>
Hello there <%= request.getRemoteHost() %>
<%
    /* end new session */
  }
  else {
    /* it's an old session */
%>
Another request from <%= request.getRemoteHost() %>
<%
   /* end old session */
  }
%>
<hr>
<form method="get" action="<%=response.encodeURL("Session2")%>">
  <input type="submit" value="Reload">
  <input type="submit" value="Logout" name="logout">
</form>
<%
  /* end not logging out */
}
%>
</body>
