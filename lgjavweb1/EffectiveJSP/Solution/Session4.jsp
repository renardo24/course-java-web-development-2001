
<%@ page import="java.util.*" %>

<%!
/* class to hold count of visits by client */
static class VisitCounter {
  int count;
  VisitCounter(int n) { count = n; }
  VisitCounter() { count = 0; }
  int visitCount() { return count; }
  void incCount() { ++count; }
}
%>

<html>
  <head>
    <title>Session 4</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>
<%
/* get visit counter stored in session or null if session
   not yet established.

   A ClassCastException will occur due to a subtle problem caused
   by the fact that every time a JSP is translated into Java a new 
   data type is created. Because the VisitCounter class is scoped
   within the generated Servlet class its actual type will change
   each time a new version of this JSP is translated. If a VisitCounter
   was stored in the session before the JSP was updated its actual
   type will be different from the expected type and the exception
   will be thrown. However, as long as we catch the exception no harm
   will result as long as it is realised that the count will be reset to 
   zero every time the JSP is modified and subsequently translated. */
VisitCounter visitCounter = null;
try {
  visitCounter = (VisitCounter)session.getAttribute("visits");
}
catch (ClassCastException e) {
  // relax
}
/* check if the logout parameter has a value of "Logout" */
String logout = request.getParameter("logout");
if(logout != null && logout.equals("Logout")) {
   /* yes, they have logged out */
   session.invalidate();
%>
Goodbye, please call again soon.
<hr>
<form method="get" action="Session4">
  <input type="submit" value="Login">
</form>
<%
  /* end logging out */
}
else {
  /* not logging out */
  if(session.isNew()) {
    /* it's a new session */
%>
Hello there <%= request.getRemoteHost() %>
<%
    /* see if the client has sent a cookie containing
       the visit count */
    Cookie[] cookies = request.getCookies();
    for(int i = 0; i < cookies.length; ++i) {
      if(cookies[i].getName().equals("qacookie")) {
        /* yes, create the visit counter initialised with the supplied value */
        visitCounter = new VisitCounter(Integer.parseInt(cookies[i].getValue()));
        /* save the visit counter in the session */
        session.setAttribute("visits", visitCounter);
        break;
      }
    }
    /* end new session */
  }
  else {
    /* not new session */
%>
Another request from <%= request.getRemoteHost() %>
<%
    /* end not new session */
  }
  /* create a visit counter if we don't already have one */
  if(visitCounter == null) {
    visitCounter = new VisitCounter();
    /* save the visit counter in the session */
    session.setAttribute("visits", visitCounter);
  }
  /* increment the count of the number of visits */
  visitCounter.incCount();
%>
<br>Total visits so far = <%= visitCounter.visitCount()%>
<hr>
<form method="get" action="<%=response.encodeURL("Session4")%>">
  <input type="submit" value="Reload">
  <input type="submit" value="Logout" name="logout">
</form>
<%
  /* save the current value of the visit counter in a cookie */
  response.addCookie(new Cookie("qacookie", "" + visitCounter.visitCount()));
}
%>
</body>
