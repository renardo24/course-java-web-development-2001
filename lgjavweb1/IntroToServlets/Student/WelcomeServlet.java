import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

// exercise one step one
// ensure that the servlet class extends HttpServlet
// declare an instance variable called "out" of type PrintWriter

public class WelcomeServlet extends HttpServlet
{
  PrintWriter out = null ;


// exercise one step two
// override doGet

public void doGet( HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
{
  this.doPost( req, res ) ;
}
// have doGet call doPost passing the appropriate parameters
// to minimize the amount of code required to process both types of request

// override doPost

public void doPost( HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
{

// exercise one step three
// set the content type for the response to text/html
// call the response object's getWriter() method
// store a reference to the writer object in the out variable declared earlier

  res.setContentType( "text/html" ) ;
  try
  {
    out = res.getWriter() ;
  }
  catch( IOException ie )
  {
    out.println( "Unable to get PrintWriter" ) ;
  }


// exercise one step four
// connect to see if a value was passed for the parameter, p2 in the request
// if no value was passed display the welcome page by calling doIntroPage
// else display the results page by calling doResultsPage

String s = req.getParameter( "p2" ) ;

if ( ( s != null ) && ( s.trim().length() != 0 ) )
  doResultsPage( req, res ) ;
else
  doIntroPage( req, res ) ;

} // end of doPost

  public void doIntroPage(HttpServletRequest req, HttpServletResponse res)
  {
    doIntroHeaders(req, res);
    doIntroBody(req, res);
    doFooter(req, res);
  }

  public void doResultsPage(HttpServletRequest req, HttpServletResponse res)
  {
    doResultsHeader(req, res);
    doResultsBody(req, res);
    doFooter(req, res);
  }

  public void doIntroHeaders(HttpServletRequest req, HttpServletResponse res)
  {

// exercise one step five
// add code to begin building the response to be sent to the client
// build the HTML from the opening <HTML> to the closing </HEAD> tag
// follow the instructions in the exercise notes for details on the page title

out.println( "<html>" ) ;
out.println( "<head><title>" ) ;
String title = getInitParameter( "orgName" ) ;
if ( ( title != null ) && ( title.trim().length() != 0 ) )
  out.println( "Welcome to " + title ) ;
else
  out.println( "Welcome to our website" ) ;
out.println( "</title></head>" ) ;


}

  public void doIntroBody(HttpServletRequest req, HttpServletResponse res)
  {
// exercise one step six
// add code which begins the body of the HTML page
// include a prompt which asks the user to submit their details
// then call doForm(...)

out.println( "<body>" ) ;
out.println( "<h2>Please enter your details:</h2>" ) ;
doForm( req, res ) ;

  }

  public void doFooter(HttpServletRequest req, HttpServletResponse res)
  {
// exercise one step seven
// add a footer to your HTML page
// include code informing the user of the time their request was processed
// Note: ensure you close all tags remaining open (i.e. don't forget </BODY> and </HTML>, etc.)

out.println( "This request was processed on: " + new Date() ) ;
out.println( "</body>" ) ;
out.println( "</html>" ) ;

}

  public void doResultsHeader(HttpServletRequest req, HttpServletResponse res)
  {
// exercise one step eight
// add code to begin building the results page to be sent to the client
// if a value exists for the orgName initialization parameter,
// use it in the title of the page else the title should be "Results Page"

out.println( "<html>" ) ;
out.println( "<head><title>" ) ;
String title = getInitParameter( "orgName" ) ;
if ( ( title != null ) && ( title.trim().length() != 0 ) )
  out.println( title ) ;
else
  out.println( "Results Page" ) ;
out.println( "</title></head>" ) ;

}

  public void doResultsBody(HttpServletRequest req, HttpServletResponse res)
  {
// Exercise one step nine
// build the body for the results page
// thank the user by name for the information they submitted
// echo the information they submitted to the page
// allow the user to resubmit their information by including a call to doForm

out.println( "<body>" ) ;
out.println( "<h2>Thank you for entering the information:</h2>" ) ;
Enumeration e = req.getParameterNames() ;
while ( e.hasMoreElements() )
{
  String st = (String) e.nextElement() ;
  out.println( "<b>" + st  + "</b>: "
               + req.getParameter( st ) ) ;
              
}
doForm( req, res ) ;
  }


  // code for doForm provided for you

  public void doForm(HttpServletRequest req, HttpServletResponse res)
  {
    out.println("<form method=POST action=\"Welcome\">");
    out.println("<p>");
    out.println("Diagnostics Displayed: <input type=checkbox name=diagnostics value=ON></p>");
    out.println("<hr>");
    out.println("<p>Name:<br>");
    out.println("<input type=text name=\"name\" size=30></p>");
    out.println("Organisation:<br>");
    out.println("<input type=text name=organisation size=30></p>");
    out.println("<p>Job Title:<br>");
    out.println("<input type=text name=jobtitle size=30></p>");
    out.println("<input type=hidden name=p2></p>");
    out.println("<p><input type=submit value=Submit><input type=reset value=Reset></p>");
    out.println("</form>");
  }
}
