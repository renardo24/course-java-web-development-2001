
set WARFILE=servletIntro.war
set EARFILE=servletIntro.ear

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive -classpath classes content web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% Welcome %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
