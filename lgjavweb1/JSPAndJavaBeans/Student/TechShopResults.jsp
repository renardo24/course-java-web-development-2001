
<jsp:useBean id="deals" class="DealsBean" >
    <jsp:setProperty name="deals" property="interest" />
    <!-- setting the default property to a string that contains a hyperlink
         is rather tricky as you can see. The attribute value has to be
         given as a scriptlet and the double quotes around Welcome need to
         be escaped twice! -->
    <jsp:setProperty name="deals" property="default"
      value='<%="Please visit the <A href=\\"Welcome\\">Welcome</A> page first"%>' />
</jsp:useBean>

<!--

exercise four step one and two
create an instance of the LocalStoresBean class
initialize the value of the area property to the
value of the passed parameter with the same name

-->
<jsp:useBean id="store" class="LocalStoresBean">
  <jsp:setProperty name="store" property="area"/>
</jsp:useBean>
<HTML>

<head>
<title></title>
</head>

<body bgcolor="#FFFF00">

<p align="center"><font face="Comic Sans MS" size="7" color="#FF0000">Welcome to</font></p>
<p align="center"><img src=image001.gif width="194" height="76"></p>
<p><font face="Comic Sans MS">Our full <a href="catalogue.html">online catalogue</a> contains hundreds of new deals.</font></P>
<P><font face="Comic Sans MS">We have several deals relating to
<jsp:getProperty name="deals" property="interest"/>
 right now. Please check to out below:</font></p>

<!--

exercise four optional step
use the instance of the DealsBean class
to display the current deals for the user's area of interest

->
<jsp:getProperty name="deals" property="deals"/>



<p>The stores nearest to you are listed below (or all stores if we're not sure where you are!):</p>


<!--

exercise four step three
use the instance of the LocalStoresBean class
to display the stores in the user's area

-->
<jsp:getProperty name="store" property="stores"/>
<HR>
Please feel free to call in for more excellent bargains.
<HR>
Processed at <%=new java.util.Date()%> by <%=request.getServerName() + " on port " + request.getServerPort()%>
</body>
</HTML>



