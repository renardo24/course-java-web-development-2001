// exercise three step one
// have the LocalStoresBean class implements the Serializable interface
// and provide a default no-argument constructor
import java.io.* ;

public class LocalStoresBean implements Serializable
{

// exercise three step two
// implement a read/write JavaBean property called Area
// of type String
  private String area = null ;

  public String getArea()
  {
    return area ;
  }

  public void setArea( String newArea )
  {
    area = newArea ;
  }

  // declare an array of Outlet Details
  private String [] addresses = 
    {
  	 "<P>1 High Street<BR> <a href=\"mailto:cardiff.admin@techshop.com\">Cardiff</a><BR>CF1  1QA<BR></P>" + 
  	 "<P>3 Castle Street<BR> <a href=\"mailto:swansea.admin@techshop.com\">Swansea</a><BR>SA1  5QA<BR></P>",
  	 
  	 "<P>5 Main Street<BR> <a href=\"mailto:belfast.admin@techshop.com\">Belfast</a><BR>BE2  1QA<BR></P>",
  	 
  	 "<P>7 Long Street<BR> <a href=\"mailto:glasgow.admin@techshop.com\">Glasgow</a><BR>GW4  9QA<BR></P>" + 
  	 "<P>17 St Michael Road<BR> <a href=\"mailto:edinburgh.admin@techshop.com\">Edinburgh</a><BR>ED4  1QA<BR></P>",

  	 "<P>4 London Road<BR> <a href=\"mailto:oxford.admin@techshop.com\">Oxford</a><BR>OX15  7QA<BR></P>" +
  	 "<P>23 London Road<BR> <a href=\"mailto:birmingham.admin@techshop.com\">Birmingham</a><BR>BR54  9QA<BR></P>" +
  	 "<P>15 Liverpool Street<BR> <a href=\"mailto:london.admin@techshop.com\">London</a><BR>W1  5QA<BR></P>",

  	 "<P>100 Lower Baggott Street<BR> <a href=\"mailto:dublin.admin@techshop.com\">Dublin</a><BR>DB1  1QA<BR></P>",

  	 "<P>1 Rue des Champes<BR> <a href=\"mailto:Nantes.admin@techshop.com\">Nantes</a><BR>France<BR></P>"
    };


// exercise three step three
// declare a read only JavaBean property called stores to return store details
// use the area property to determine which stores should be returned
// a no area or an unknown area is provided return all addresses

public String getStores()
{
    if ( area.equalsIgnoreCase( "wales" ) )
      return addresses [0] ;
    else if ( area.equalsIgnoreCase( "ni" ) )
      return addresses [1] ;
    else if ( area.equalsIgnoreCase( "scotland" ) )
      return addresses [2] ;
    else if ( area.equalsIgnoreCase( "england" ) )
      return addresses [3] ;
    else if ( area.equalsIgnoreCase( "ireland" ) )
      return addresses [4] ;
    else if ( area.equalsIgnoreCase( "france" ) )
      return addresses [5] ;
    else
    {
      StringBuffer buf = new StringBuffer( "<h2>All addresses:</h2>") ;
      for ( int i = 0; i < addresses.length; i++ )
        buf.append( addresses[i] ) ;
      return buf.toString() ;
    }
}

}
