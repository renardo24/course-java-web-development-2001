import java.io.*;


public class LocalStoresBean implements Serializable
{
  public LocalStoresBean()
  {
  }


  private String area;

  public void setArea(String newArea)
  {
    area = newArea;
  }


  public String getArea()
  {
    return area;
  }


  // declare an array of Outlet Details
  private String [] addresses = 
    {
  	 "<P>1 High Street<BR> <a href=\"mailto:cardiff.admin@techshop.com\">Cardiff</a><BR>CF1  1QA<BR></P>" + 
  	 "<P>3 Castle Street<BR> <a href=\"mailto:swansea.admin@techshop.com\">Swansea</a><BR>SA1  5QA<BR></P>",
  	 
  	 "<P>5 Main Street<BR> <a href=\"mailto:belfast.admin@techshop.com\">Belfast</a><BR>BE2  1QA<BR></P>",
  	 
  	 "<P>7 Long Street<BR> <a href=\"mailto:glasgow.admin@techshop.com\">Glasgow</a><BR>GW4  9QA<BR></P>" + 
  	 "<P>17 St Michael Road<BR> <a href=\"mailto:edinburgh.admin@techshop.com\">Edinburgh</a><BR>ED4  1QA<BR></P>",
  	 
  	 "<P>4 London Road<BR> <a href=\"mailto:oxford.admin@techshop.com\">Oxford</a><BR>OX15  7QA<BR></P>" + 
  	 "<P>23 London Road<BR> <a href=\"mailto:birmingham.admin@techshop.com\">Birmingham</a><BR>BR54  9QA<BR></P>" +
  	 "<P>15 Liverpool Street<BR> <a href=\"mailto:london.admin@techshop.com\">London</a><BR>W1  5QA<BR></P>",
  	 
  	 "<P>100 Lower Baggott Street<BR> <a href=\"mailto:dublin.admin@techshop.com\">Dublin</a><BR>DB1  1QA<BR></P>",
  	 
  	 "<P>1 Rue des Champes<BR> <a href=\"mailto:Nantes.admin@techshop.com\">Nantes</a><BR>France<BR></P>"
    };
  				
					
   // declare a method to return store details given an area
   public String getStores()
   {
     if("wales".equalsIgnoreCase(area))
     {
     	return addresses[0];
     }
     if("ni".equalsIgnoreCase(area))
     {
      	return addresses[1];
     }
     if("scotland".equalsIgnoreCase(area))
     {
      	return addresses[2];
     }
     if("england".equalsIgnoreCase(area))
     {
      	return addresses[3];
     }
     if("ireland".equalsIgnoreCase(area))
     {
      	return addresses[4];
     }
     if("france".equalsIgnoreCase(area))
     {
      	return addresses[5];
     }
     
     
     String adds =  "<H3>All addresses</H3>";
     
     for(int i=0;i<addresses.length;i++)
     {
       adds+=addresses[i];
     }
     return adds;
   }

}
