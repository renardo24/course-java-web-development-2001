
set WARFILE=jspBeans.war
set EARFILE=jspBeans.ear

set CLASSES=DealsBean.class:TipBean.class:LocalStoresBean.class
set CONTENT=TechShopWelcome.jsp:TechShopResults.jsp:image001.gif:catalogue.html:underconstruction.html

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive -classpath . -classFiles %CLASSES% . -contentFiles %CONTENT% web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% jspBeans %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
