public class TipBean
{

  private String [] tips = { 
  				"Don't touch wires to see if they're live!",
				"Adding 'throws Exception' to a <B>main</B> method is not 'error handling'.",
				"JSPs really are this simple.",
				"Take a coffee break if you've got this to work.",
				"If all else fails, compile from the command line.",
				"This would be tougher to write in a servlet.",
				"Never order a curd tart when drunk."
			   };

								
   // declare a method to return a random tip of the day
   
   public String getRandomTip()
   {
     int selector = (((int)(Math.random()*1000))%tips.length);
     return tips[selector];
   }

}