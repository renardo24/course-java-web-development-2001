<HTML>
<head>
<title></title>
</head>

<body bgcolor="#FFFF00">

<p align="center"><font face="Comic Sans MS" size="7" color="#FF0000">Welcome to</font></p>
<p align="center"><img src=image001.gif width="194" height="76"></p>
<p><font face="Comic Sans MS">Please feel free to browse our <a href="catalogue.html">online catalogue</a>.
You'll find many bargains just waiting for you.</font></p>
<p><font face="Comic Sans MS"><font color="#0000FF">Tip of the Day:&nbsp;<br>
</font><font color="#000000"><br>

<%=getTip()%>

</font></font></p>
<p>To find the store nearest you, please complete the form below:</p>

<form method="POST" action="Results">
  <p>Name: <input type="text" name="T1" size="20"><br>
  Location: <select size="1" name="area">
    <option selected value="all">All Locations</option>
    <option value="wales">Wales</option>
    <option value="scotland">Scotland</option>
    <option value="england">England</option>
    <option value="ni">Northern Ireland</option>
    <option value="ireland">Ireland</option>
    <option value="france">France</option>
  </select><br>
  Interested in: <select size="1" name="interest">
    <option value="hard disks">Hard Disks</option>
    <option value="RAM">RAM</option>
    <option value="video cards">Video Cards</option>
    <option value="processors">Processors</option>
    <option value="technology">Other Stuff</option>
  </select></p>
  <p><input type="submit" value="Submit" name="B1"><input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<HR>
Processed at <%=new java.util.Date()%> by <%=request.getServerName() + ":" + request.getServerPort()%>

</body>
<%!
  // Declare an array of Tips of the day
  private String [] tips = { 
  				"Don't touch wires to see if they're live!",
				"Adding 'throws Exception' to a <B>main</B> method is not 'error handling'.",
				"JSPs really are this simple.",
				"Take a coffee break if you've got this to work.",
				"If all else fails, compile from the command line.",
				"This would be tougher to write in a servlet.",
				"Never order a curd tart when drunk."
			   };

								
   // declare a method to return a random tip of the day
   
   public String getTip()
   {
     int selector = (((int)(Math.random()*1000))%tips.length);
     return tips[selector];
   }
   // 
 
%>

</HTML>

