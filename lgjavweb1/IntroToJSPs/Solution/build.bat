
set WARFILE=jspIntro.war
set EARFILE=jspIntro.ear

set CONTENT=TechShopWelcome.jsp:TechShopResults.jsp:image001.gif:catalogue.html

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive . -contentFiles %CONTENT% web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% jspIntro %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
