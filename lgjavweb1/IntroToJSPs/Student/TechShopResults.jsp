<HTML>

<head>
<title></title>
</head>

<body bgcolor="#FFFF00">

<p align="center"><font face="Comic Sans MS" size="7" color="#FF0000">Welcome to</font></p>
<p align="center"><img src=image001.gif width="194" height="76"></p>
<p><font face="Comic Sans MS">Our full <a href="catalogue.html">online catalogue</a> contains hundreds of new deals.</font></P>
<P><font face="Comic Sans MS">We have several deals relating to

<!--

exercise two step seven
add a Java scriptlet to include the value of the interest parameter in the page

-->
<%
 if ( request.getParameter( "interest" ) != null )
  out.println( request.getParameter( "interest" ) ) ;
 else out.println( "technology" ) ;
%>

 right now. Please check them out below:</font></p>

<!--

exercise two step eight
add a Java scriptlet to include the deals in the page
Pass the value of the interest parameter in to the getDeals() method

-->
<%= getDeals ( request.getParameter( "interest" ) ) %>



<p>The stores nearest to you are listed below (or all stores if we're not sure where you are!):</p>

<!--

exercise two step nine
add a Java scriptlet to include the local store addresses in the page
Pass the value of the area parameter in to the getStores() method

-->
<%= getStores( request.getParameter( "area" ) ) %>

<HR>
Please feel free to call in for more excellent bargains.
<HR>
Processed at <%=new java.util.Date()%> by <%=request.getServerName() + ":" + request.getServerPort()%>
</body>

<!--
exercise two step one
add a JSP declaration
Within the scope of the declaration define a String array called addresses
initialise the array using the values in addresses.txt
-->
<%!
    private String[] addresses =
    {
   	 "<P>1 High Street<BR> <a href=\"mailto:cardiff.admin@techshop.com\">Cardiff</a><BR>CF1  1QA<BR></P>" +
  	 "<P>3 Castle Street<BR> <a href=\"mailto:swansea.admin@techshop.com\">Swansea</a><BR>SA1  5QA<BR></P>",

  	 "<P>5 Main Street<BR> <a href=\"mailto:belfast.admin@techshop.com\">Belfast</a><BR>BE2  1QA<BR></P>",

  	 "<P>7 Long Street<BR> <a href=\"mailto:glasgow.admin@techshop.com\">Glasgow</a><BR>GW4  9QA<BR></P>" +
  	 "<P>17 St Michael Road<BR> <a href=\"mailto:edinburgh.admin@techshop.com\">Edinburgh</a><BR>ED4  1QA<BR></P>",

  	 "<P>4 London Road<BR> <a href=\"mailto:oxford.admin@techshop.com\">Oxford</a><BR>OX15  7QA<BR></P>" +
  	 "<P>23 London Road<BR> <a href=\"mailto:birmingham.admin@techshop.com\">Birmingham</a><BR>BR54  9QA<BR></P>" +
  	 "<P>15 Liverpool Street<BR> <a href=\"mailto:london.admin@techshop.com\">London</a><BR>W1  5QA<BR></P>",

  	 "<P>100 Lower Baggott Street<BR> <a href=\"mailto:dublin.admin@techshop.com\">Dublin</a><BR>DB1  1QA<BR></P>",

  	 "<P>1 Rue des Champes<BR> <a href=\"mailto:Nantes.admin@techshop.com\">Nantes</a><BR>France<BR></P>"
    } ;

%>

<!--
exercise two step two
In addition to the addresses array, declare a single method called getStores
getStores should return a string and accept a single String parameter, locale

exercise two step three
Implement getStores to return the appropriate String from the addresses
array, based upon the locale of the user. If no locale is specified, or an unknown locale
return the addresses of all stores
-->

<%!
  public String getStores( String locale )
  {
    if ( locale.equalsIgnoreCase( "wales" ) )
      return addresses [0] ;
    else if ( locale.equalsIgnoreCase( "ni" ) )
      return addresses [1] ;
    else if ( locale.equalsIgnoreCase( "scotland" ) )
      return addresses [2] ;
    else if ( locale.equalsIgnoreCase( "england" ) )
      return addresses [3] ;
    else if ( locale.equalsIgnoreCase( "ireland" ) )
      return addresses [4] ;
    else if ( locale.equalsIgnoreCase( "france" ) )
      return addresses [5] ;
    else
    {
      StringBuffer buf = new StringBuffer( "<h2>All addresses:</h2>") ;
      for ( int i = 0; i < addresses.length; i++ )
        buf.append( addresses[i] ) ;
      return buf.toString() ;
    }
  }

%>
<!--
exercise two step four
define a String array called deals
initialise the array using the values in deals.txt
-->

<%!
  private String[] deals =
  {
// General Technology
       "<A href=underconstruction.html>Full System - Celeron 600MHz</A> - �975<BR>" +
       "<A href=underconstruction.html>Full System - PIII 800MHz</A> - �1225<BR>" +
       "<A href=underconstruction.html>Full System - Athlon 850MHz</A> - �1450<BR>" +
       "<A href=underconstruction.html>Full System - Athlon 1100MHz</A> - �1950<HR>",
// Hard Disks
       "<A href=underconstruction.html>15GB hard drive</A> - �100<BR>" +
       "<A href=underconstruction.html>30GB hard drive</A> - �160<BR>" +
       "<A href=underconstruction.html>50GB hard drive</A> - �220<BR>" +
       "<A href=underconstruction.html>75GB hard drive</A> - �350<HR>",
// RAM
       "<A href=underconstruction.html>64MB PC100 DIMM</A> - �50<BR>" +
       "<A href=underconstruction.html>128MB PC100 DIMM</A> - �100<BR>" +
       "<A href=underconstruction.html>256MB PC100 DIMM</A> - �220<BR>" +
       "<A href=underconstruction.html>512MB PC100 DIMM</A> - �520<HR>",
// Video Cards
       "<A href=underconstruction.html>16MB Video Card</A> - �70<BR>" +
       "<A href=underconstruction.html>32MB Video Card</A> - �110<BR>" +
       "<A href=underconstruction.html>64MB Video Card</A> - �220<HR>",
// processors
       "<A href=underconstruction.html>700MHz Athlon</A> - �150<BR>" +
       "<A href=underconstruction.html>800MHz Athlon</A> - �200<BR>" +
       "<A href=underconstruction.html>850MHz Athlon</A> - �250<BR>" +
       "<A href=underconstruction.html>950MHz Athlon</A> - �450<BR>" +
       "<A href=underconstruction.html>1100MHz Athlon</A> - �650<HR>"
  } ;
%>

<!--
exercise two step five
declare a single method called getDeals
getDeals should return a string and accept a single String parameter, interest

exercise two step six
Implement getDeals to return a String from the deals array based upon the
customer's area of interest.
If no interest or an unknown interest is specified, return a hyperlink to the
Welcome page allowing the user to complete the form.
-->
<%!
  public String getDeals( String interest )
  {
    if ( interest.equalsIgnoreCase( "technology" ) )
      return deals[0] ;
    else if ( interest.equalsIgnoreCase( "hard disks" ) )
      return deals[1] ;
    else if ( interest.equalsIgnoreCase( "ram" ) )
      return deals[2] ;
    else if ( interest.equalsIgnoreCase( "video cards" ) )
      return deals[3] ;
    else if ( interest.equalsIgnoreCase( "processors" ) )
      return deals[4] ;
    else
      return "Please let us know of your interests on our <A href=\"Welcome\">welcome page</A>";

  }
%>
</HTML>



