<HTML>
<head>
<title></title>
</head>

<body bgcolor="#FFFF00">

<p align="center"><font face="Comic Sans MS" size="7" color="#FF0000">Welcome to</font></p>
<p align="center"><img src=image001.gif width="194" height="76"></p>
<p><font face="Comic Sans MS">Please feel free to browse our <a href="catalogue.html">online catalogue</a>.
You'll find many bargains just waiting for you.</font></p>
<p><font face="Comic Sans MS"><font color="#0000FF">Tip of the Day:&nbsp;<br>
</font><font color="#000000"><br>

<!--

exercise one step four
add a Java scriptlet to include a tip of the day in the page

-->
<% out.println( getTip() ) ; %>

</font></font></p>
<p>To find the store nearest you, please complete the form below:</p>

<!--

exercise one step five
add a form to the HTML which will submit its contents to
the Results page through HTTP POST
-->
<form method="POST" action="Results">
<p>Please select an area:
<select size="5" name="area">
 <option selected value="france">France</option>
 <option selected value="ni">Northern Ireland</option>
 <option selected value="ireland">Ireland</option>
 <option selected value="england">England</option>
 <option selected value="scotland">Scotland</option>
 <option selected value="wales">Wales</option>
</select>
</p>
<p>Please select an interest:
<select size="3" name="interest">
 <option selected value="hard disks">Hard disks</option>
 <option selected value="video cards">Video cards</option>
 <option selected value="ram">RAM</option>
 <option selected value="processors">Processors</option>
 <option selected value="technology">Technology</option>
</select>
</p>

<input type="submit" value="Submit data">
</form>

<!--
exercise one step six
populate the form with appropriate HTML controls for submitting the users area
and interest. select controls would be good candidates for this particular form

-->
<p></p>
<p></p>
<HR>

<!--
exercise one optional extra
Output a line that looks like the following with suitable information
replacing the descriptions in brackets
Processed at [current date and time] by [name of server]:[port number]
-->

</body>

<!--

exercise one step one
add a JSP declaration 
Within the scope of the declaration define a String array called tips
Initialize this array with several HTML-based tips
<%!
  String[] tips =
      {
          "<h1>Tip number 1</h1>" ,
          "<h2>Tip number 2</h2>" ,
          "<h3>Tip number 3</h3>" ,
          "<h4>Tip number 4</h4>" ,
          "<h5>Tip number 5</h5>" ,
          "<h6>Tip number 6</h6>" ,
      } ;
%>

exercise one step two
In addition to the tips array, declare a single method called getTip
getTip should return a string and accept no parameters

exercise one step three
Implement getTip to return a single tip randomly selected from the array
<%!
      String getTip()
      {
        int index = ( ( (int) ( Math.random()*1000 ) ) % tips.length ) ;
        return tips[index] ;
      }
%>

-->

</HTML>

