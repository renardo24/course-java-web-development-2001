package gossipEJB;

import java.rmi.RemoteException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import java.util.*;


public class GossipEJB implements SessionBean {
 
   final static Vector theGossip = new Vector();

   public void submitGossip(String gossip) throws RemoteException
   {
      theGossip.add(gossip);
   }

   public String getGossip() throws RemoteException
   {
      String results = "<HR>";
      for(Enumeration e = theGossip.elements() ; e.hasMoreElements() ; )
      {
         results+=(e.nextElement() + "<BR>");
      }
      return results+="<HR>";
   }

   public void ejbCreate() {}
   public void ejbRemove() {}
   public void ejbActivate() {}
   public void ejbPassivate() {}
   public void setSessionContext(SessionContext sc) {}
} 

