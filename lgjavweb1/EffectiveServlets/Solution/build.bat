
set WAR_FILE=effectiveWebApp.war
set EAR_FILE=effectiveApp.ear

del %WAR_FILE%
del %EAR_FILE%

set CLASS_FILES=effective/GossipServlet.class:effective/HeaderServlet.class:effective/MainServlet.class:effective/NewsServlet.class:effective/RouterServlet.class
set CONTENT_FILES=images/BD10297_.GIF:images/BD10298_.GIF:images/BD10299_.GIF:images/BD10300_.GIF:images/BD10301_.GIF:images/BD10302_.GIF:images/BD10335_.GIF:html/order.htm

call %J2EE_HOME%\bin\packager -webArchive -classpath . -classFiles %CLASS_FILES% . -contentFiles %CONTENT_FILES% effective/web.xml %WAR_FILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WAR_FILE%:GossipEJB.jar EffectiveApp %EAR_FILE%
call %J2EE_HOME%\bin\packager -setRuntime %EAR_FILE% sun-j2ee-ri.xml
