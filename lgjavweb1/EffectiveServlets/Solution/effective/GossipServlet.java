package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import javax.naming.*;
import javax.rmi.*;
import java.io.*;
import gossipEJB.*;


public class GossipServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
    rsp.setContentType("text/html");
    try
    {
      out = rsp.getWriter();

      out.println("<html>");
      out.println("<head>");
      out.println("<title>Welcome to Gossip.com</title>");
      out.println("</head>");
      out.println("<body bgcolor=#CCFF66>");


      ServletContext ctx = getServletContext();
      String url = "/HeaderServlet";
      RequestDispatcher rd = ctx.getRequestDispatcher(url);
      rd.include(req, rsp);

      out.println("<p align=center><font face=\"Comic Sans MS\" size=6 color=#FF0000>Gossip.com</font></p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>All submitted gossip is listed below: ");
      out.println("</font></p>");

      String subGos = req.getParameter("gossip");

      // The next section of code is used to contact an EJB
      try{
        // get initial context for making JNDI lookup calls
        InitialContext ic = new InitialContext();

        // lookup GossipEJB Home
        Object objref = ic.lookup("java:comp/env/ejb/Gossip");
        // cast to proper interface type
        GossipHome gossipHome = (GossipHome)PortableRemoteObject.narrow(objref, GossipHome.class);

        // use GossipHome to create a new instance of GossipEJB
        Gossip remoteGossip = gossipHome.create();

        // If Gossip was passed to this servlet, submit it to the GossipEJB
        if(subGos != null)
        {
           // submit the client's gossip to the Enterprise Java Bean
           remoteGossip.submitGossip(subGos);
        }

        // Get all the gossip back from the GossipEJB and write to response
        String allGossip = remoteGossip.getGossip();
        out.println(allGossip);

      }catch(Exception ejbEx){
        out.println("Problem during contact of EJB <BR>" + ejbEx.toString());
      }

      out.println("<p>&nbsp;</p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>If you know some juicy gossip, submit it using the form below: ");
      out.println("</font></p>");

      out.println("<HR><FORM METHOD=POST ACTION=GossipServlet>");
      out.println("Please type your gossip here: <INPUT NAME=gossip><BR><INPUT TYPE=SUBMIT VALUE=Submit></FORM>");


      out.println("</body>");
      out.println("</html>");

    }
    catch(Exception e)
    {
      System.out.println(e);
      e.printStackTrace(System.out);
    }
  }
 
}
