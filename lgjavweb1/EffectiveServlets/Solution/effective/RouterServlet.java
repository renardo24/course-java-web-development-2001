package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;


public class RouterServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {

    try
    {


      String page = req.getParameter("page");
      System.out.println("page="+page);
      ServletContext ctx = getServletContext();
      String url  = null;
      RequestDispatcher rd = null;


      if("news".equalsIgnoreCase(page))
      {
        url = "/NewsServlet";
        rd = ctx.getRequestDispatcher(url);
        rd.forward(req, rsp);
        return;
      }

      if("order".equalsIgnoreCase(page))
      {
        url = "/html/order.htm";
        rd = ctx.getRequestDispatcher(url);
        rd.forward(req, rsp);
        return;
      }

      if("gossip".equalsIgnoreCase(page))
      {
        url = "/GossipServlet";
        rd = ctx.getRequestDispatcher(url);
        rd.forward(req, rsp);
        return;
      }

      url = "/MainServlet";
      rd = ctx.getRequestDispatcher(url);
      rd.forward(req, rsp);
    }
    catch(Exception e)
    {
      System.out.println(e);
      e.printStackTrace(System.out);
    }
  }
 
}