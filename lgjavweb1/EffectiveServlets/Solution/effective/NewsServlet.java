package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;


public class NewsServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
    rsp.setContentType("text/html");
    try
    {
      out = rsp.getWriter();

      out.println("<html>");
      out.println("<head>");
      out.println("<title>Latest News at Gossip.com</title>");
      out.println("</head>");
      out.println("<body bgcolor=#CCFF66>");


      ServletContext ctx = getServletContext();
      String url = "/HeaderServlet";
      RequestDispatcher rd = ctx.getRequestDispatcher(url);
      rd.include(req, rsp);

      out.println("<p align=center><font face=\"Comic Sans MS\" size=6 color=#FF0000>Gossip.com</font></p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>Under Construction");
      out.println("</font></p>");
      out.println("<ul>");
      out.println("  <li><font face=\"Comic Sans MS\" size=5>News Item 1</font></li>");
      out.println("  <li><font face=\"Comic Sans MS\" size=5>News Item 2</font></li>");
      out.println("  <li><font face=\"Comic Sans MS\" size=5>News Item 3</font></li>");
      out.println("  <li><font face=\"Comic Sans MS\" size=5>Recent News</font></li>");
      out.println("</ul>");

      out.println("<p>&nbsp;</p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>If you know some juicy gossip, please let");
      out.println("us know on the Gossip Page</font></p>");
      out.println("</body>");
      out.println("</html>");

    }
    catch(Exception e)
    {
      System.out.println(e);
      e.printStackTrace(System.out);
    }
  }
 
}