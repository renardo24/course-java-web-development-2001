package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;



public class HeaderServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {

    try
    {

      // exercise one step three
      // Obtain reference to the response writer
      // Write out as a response the content between
      // the HTML comments in the order.htm page
      PrintWriter out = rsp.getWriter() ;
      out.println( "<table BORDER=0 CELLPADDING=0 WIDTH=\"100%\">" );
      out.println( "<tr>" );
      out.println( "<td WIDTH=\"25%\"><a href=\"MainServlet\"><img SRC=\"images/BD10298_.GIF\"></a><a href=\"MainServlet\">Home</a></td>" );
      out.println( "<td WIDTH=\"25%\"><a href=\"RouterServlet?page=news\"><img SRC=\"images/BD10298_.GIF\"></a><a href=\"RouterServlet?page=news\">Latest News</a></td>" );
      out.println( "<td WIDTH=\"25%\"><a href=\"RouterServlet?page=order\"><img SRC=\"images/BD10298_.GIF\"></a><a href=\"RouterServlet?page=order\">Place an Order</a></td>" );
      out.println( "<td WIDTH=\"25%\"><a href=\"RouterServlet?page=gossip\"><img SRC=\"images/BD10298_.GIF\"></a><a href=\"RouterServlet?page=gossip\">Gossip&nbsp;</a></td>" );
      out.println( "</tr>" );
      out.println( "</table>" );
    }
    catch(IOException e)
    {
      System.out.println("Unable to acquire Writer for servlet:\n" + e);
    }
  }
 
}