package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;


public class RouterServlet extends HttpServlet
{

  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
    try
    {
      // exercise two step one
      // retrieve the value of the "page" request parameter
      String thePage = req.getParameter( "page" ) ;

      // exercise two step two
      // declare a ServletContext variable and store in it a reference 
      // to the ServletContext. Also declare a RequestDispatcher variable
      ServletContext sc = getServletContext() ;
      RequestDispatcher rd = null ;

      // exercise two step three
      // if the page parameter's value is equal to "news",
      // acquire the request dispatcher for /NewsServlet and forward the request to it
      if ( thePage.equalsIgnoreCase( "news" ) )
      {
        rd = sc.getRequestDispatcher( "/NewsServlet" ) ;
        rd.forward( req, rsp );
      }

      // exercise two step four
      // if the page parameter's value is equal to "order",
      // acquire the request dispatcher for /html/order.htm and forward the request to it
      else if ( thePage.equalsIgnoreCase( "order" ) )
      {
        rd = sc.getRequestDispatcher( "/html/order.htm" ) ;
        rd.forward( req, rsp );
      }

      // exercise two step five
      // if the page parameter's value is equal to "gossip",
      // acquire the request dispatcher for /GossipServlet and forward the request to it
      else if ( thePage.equalsIgnoreCase( "gossip" ) )
      {
        rd = sc.getRequestDispatcher( "/GossipServlet" ) ;
        rd.forward( req, rsp );
      }

      // exercise two step six
      // If no forward has occurred at this point, forward the request to /MainServlet
      else
      {
        rd = sc.getRequestDispatcher( "/MainServlet" ) ;
        rd.forward( req, rsp );
      }


    }
    catch(Exception e)
    {
      System.out.println(e);
      e.printStackTrace(System.out);
    }
  }
 
}