package gossipEJB;

import java.rmi.RemoteException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import java.util.*;


public class GossipEJB implements SessionBean {
 
   static Vector theGossip = new Vector();

   public void submitGossip(String gossip) throws RemoteException
   {
      theGossip.add(gossip);
   }

   public String getGossip() throws RemoteException
   {
      String results = "<HR>", item;
      Enumeration gos = theGossip.elements();
      for(Enumeration e = theGossip.elements() ; e.hasMoreElements() ; ) 
      {
         results+=(e.nextElement() + "<BR>");
      }
      return results+="<HR>";
   }

   public GossipEJB() {}
   public void ejbCreate() {}
   public void ejbRemove() {}
   public void ejbActivate() {}
   public void ejbPassivate() {}
   public void setSessionContext(SessionContext sc) {}
} 

