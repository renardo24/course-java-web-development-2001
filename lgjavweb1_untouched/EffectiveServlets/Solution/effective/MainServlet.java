package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;


public class MainServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
    rsp.setContentType("text/html");
    try
    {
      out = rsp.getWriter();

      out.println("<html>");
      out.println("<head>");
      out.println("<title>Welcome to Gossip.com</title>");
      out.println("</head>");
      out.println("<body bgcolor=#CCFF66>");

      
      ServletContext ctx = getServletContext();
      String url = "/HeaderServlet";
      RequestDispatcher rd = ctx.getRequestDispatcher(url);
      rd.include(req, rsp);

      out.println("<p align=center><font face=\"Comic Sans MS\" size=6 color=#FF0000>Gossip.com</font></p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>You'll find gossip, shopping, news and");
      out.println("current affairs on this site. </font></p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>Use the links above to find what your");
      out.println("looking for. </font></p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p>&nbsp;</p>");
      out.println("<p><font face=\"Comic Sans MS\" size=5>If you know some juicy gossip, please let");
      out.println("us know on the Gossip Page</font></p>");
      out.println("</body>");
      out.println("</html>");

    }
    catch(Exception e)
    {
      System.out.println(e);
      e.printStackTrace(System.out);
    }
  }
 
}