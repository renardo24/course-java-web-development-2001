package effective;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;



public class HeaderServlet extends HttpServlet
{
  
  PrintWriter out = null;

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {
    doPost(req, rsp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {

    try
    {
      out = rsp.getWriter();
      out.println("<hr>");
      out.println("<table border=0 width=100% cellpadding=0>");
      out.println("  <tr>");
      out.println("    <td width=25%><a href=\"MainServlet\"><img src=\"images/BD10298_.GIF\"> Home</a></td>");
      out.println("    <td width=25%><a href=\"RouterServlet?page=news\"><img src=\"images/BD10298_.GIF\" width=15 height=15>");
      out.println("      Latest News</a></td>");
      out.println("    <td width=25%><a href=\"RouterServlet?page=order\"><img src=\"images/BD10298_.GIF\">");
      out.println("      Place an Order</a></td>");
      out.println("    <td width=25%><a href=\"RouterServlet?page=gossip\"><img src=\"images/BD10298_.GIF\">");
      out.println("      Gossip </a></td>");
      out.println("  </tr>");
      out.println("</table>");
      out.println("<hr>");
    }
    catch(IOException e)
    {
      System.out.println("Unable to acquire Writer for servlet:\n" + e);
    }
  }
 
}