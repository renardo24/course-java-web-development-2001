package gossipEJB;

import javax.ejb.EJBObject;
import java.rmi.RemoteException;

public interface Gossip extends EJBObject {
 
   public void submitGossip(String gossip) throws RemoteException;
   public String getGossip() throws RemoteException;
}

