package gossipEJB;

import java.io.Serializable;
import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface GossipHome extends EJBHome {

    Gossip create() throws RemoteException, CreateException;
}

