
set WARFILE=effectiveJsp.war
set EARFILE=effectiveJsp.ear

set CONTENT=Session1.jsp:Session2.jsp:Session3.jsp:Session4.jsp

del %WARFILE%
call %J2EE_HOME%\bin\packager -webArchive . -contentFiles %CONTENT% web.xml %WARFILE%
del %EARFILE%
call %J2EE_HOME%\bin\packager -enterpriseArchive %WARFILE% effectiveJsp %EARFILE%
call %J2EE_HOME%\bin\packager -setRuntime %EARFILE% sun-j2ee-ri.xml
