
<%@ page import="java.util.*" %>

<%!
/* class to hold count of visits by client */
static class VisitCounter {
  int count;
  VisitCounter() { count = 0; }
  int visitCount() { return count; }
  void incCount() { ++count; }
}
%>

<html>
  <head>
    <title>Session 3</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>
<%
/* check if the logout parameter has a value of "Logout" */
String logout = request.getParameter("logout");
if(logout != null && logout.equals("Logout")) {
   /* yes, they have logged out */
   session.invalidate();
%>
Goodbye, please call again soon.
<hr>
<form method="get" action="Session3">
  <input type="submit" value="Login">
</form>
<%
  /* end logging out */
}
else {
  /* not logging out */
  VisitCounter visitCounter = null;
  try {
    visitCounter = (VisitCounter)session.getAttribute("visits");
  }
  catch (ClassCastException e) {
    // relax
  }
  if(visitCounter == null) {
    visitCounter = new VisitCounter();
    session.setAttribute("visits", visitCounter);
  }
  visitCounter.incCount();
  if(session.isNew()) {
    /* it's a new session */
%>
Hello there <%= request.getRemoteHost() %>
<%
    /* end new session */
  }
  else {
    /* it's an old session */
%>
Another request from <%= request.getRemoteHost() %>
<%
    /* end old session */
  }
%>
<br>Visits so far this session = <%= visitCounter.visitCount()%>
<hr>
<form method="get" action="<%=response.encodeURL("Session3")%>">
  <input type="submit" value="Reload">
  <input type="submit" value="Logout" name="logout">
</form>
<%
  /* end not logging out */
}
%>
</body>
