
<%@ page import="java.util.*" %>

<html>
  <head>
    <title>Session 1</title>
  </head>
  <body>
    <h1>Hello World at <%= new Date()%></h1>
<%
/* test to see if the session is new and modify
   the response accordingly */
if(session.isNew()) {
  /* it's a new session */
%>
Hello there <%= request.getRemoteHost() %>
<%
  /* end new session */
}
else {
  /* it's an old session */
%>
Another request from <%= request.getRemoteHost() %>
<%
  /* end old session */
}
%>
  </body>
</html>
