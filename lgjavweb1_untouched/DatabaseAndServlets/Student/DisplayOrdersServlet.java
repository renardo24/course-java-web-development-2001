import javax.servlet.http.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import javax.servlet.*;
import java.io.*;

// exercise two step one
// have the DisplayOrdersServlet class subclass HttpServlet and
// also implement the single thread model

public class DisplayOrdersServlet
{

// exercise two step two
// declare private variables to hold references to:
// a JDBC Connection object
// a JDBC Statement object
// a JDBC ResultSet object; and
// a PrintWriter object called out


  public void init()
  {

// exercise two step three
// use JNDI to obtain a connection to the database "jdbc/ServletDB"


  }

  public void doGet(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step four
// have doGet simply pass through the request to doPost

  }

  public void doPost(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step five
// set the response's content type
// retrieve the writer for the response object and store a reference to it in
// the out PrintWriter variable
// if no id parameter is present, call the doError method passing the appropriate parameters
// otherwise, call doHeader, mainTitle, doOrderTable and doFooter in that order


  }

  public void doError(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step six
// build a full response which reports the problem to the user
// provide them with a hyperlink back to the login page to enable
// them to fill out the form and re-submit it

  }

  public void doHeader(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step seven
// Build up the response page from the opening <HTML> tag to the close of the </HEAD> tag
// Include a title which describes the customer by name or id as appropriate





  }

  public void mainTitle(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step eight
// open the HTML body tag and provide a Header which notes which customer we are
// displaying order history for. Break up the page with the inclusion of a horizontal line

  }

  public void getData(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise two step nine
// build the SQL to be sent to the database
// append a where clause and search by customer id (custid)
// if one is provided, else add a where clause which searches by forename and surname
// if you are unfamiliar with SQL, a solution to this step is in the file sql.txt



// exercise two step ten
// create a Statement for use with the connection we have acquired
// Use the statement object to execute the query and store the returned ResultSet reference
// in the ResultSet variable declared in step two of the exercise. Add appropriate exception handling.


  }

  public void doOrderTable(HttpServletRequest req, HttpServletResponse rsp)
  {
    getData(req, rsp);
    try
    {
// exercise two step eleven
// add code which builds an HTML table of results based on the ResultSet returned in getData()


    }
    catch(Exception e)
    {
      System.out.println("Data Access Error: " + e);
    }
  }

  public void doFooter(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise two step twelve
// complete the page, ensuring that no tags are left unclosed.


  }

}
