import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import javax.servlet.http.*;
import java.io.*;

public class SetupDBServlet extends HttpServlet {
  PrintWriter out;

  public void doPost(HttpServletRequest req, HttpServletResponse resp) {
    doGet(req, resp);
  }

  public void doGet(HttpServletRequest req, HttpServletResponse resp) {

    // step one
    // obtain a PrintWriter to output to

    // step two
    // set the content type to text/plain

    try {
      // step three
      // obtain the initial context

      // step four
      // obtain a DataSource for the database called jdbc/ServletDB

      // step five
      // obtain a Connection from the DataSource

      // step six
      // obtain a Statement from the Connection

      // step seven
      // execute all of the SQL statements in the arrays called sqlIgnoreErrors
      // ignoring any errors that may occur

      // step eight
      // execute all of the SQL statements in the arrays called sqlIgnoreErrors
      // catching and reporting any errors that may occur

      out.println("Database initialised OK");
    }
    catch(Exception e) {
      e.printStackTrace(out);
    }
  }
  
  private String [] sqlIgnoreErrors = {
    "DROP TABLE Customer",
    "DROP TABLE Orders",
    "DROP TABLE ProductCategories",
    "DROP TABLE Product",
  };

  private String [] sql = {
    "CREATE TABLE Customer ( custid int NOT NULL , Forename varchar (50) NOT NULL , MiddleInitial char (1) NULL , Surname varchar (50) NOT NULL , ContactNumber varchar (20) NULL , HouseNumber char (30) NULL , PostCode char (10) NULL ) ",
    "insert into customer values(1, 'Paul', 'J', 'Storer-Martin', '02920 111111', '1', 'CF99 9ZZ  ')",
    "insert into customer values(2, 'Iain', 'M', 'Banks', '08888 222222', '99', 'ED99 9XX  ')",
    "insert into customer values(3, 'Fred', 'Q', 'Bloggs', '01111 333333', '333', 'FB25 5JJ  ')",
    "insert into customer values(4, 'John', 'X', 'Smith', '02222 333666', '12', 'JS99 1LL  ')",
    "insert into customer values(5, 'Mike', 'N', 'Mechanics', '05555 555555', '2', 'NN44 2NR  ')",
    "insert into customer values(6, 'Steve', 'L', 'Smith', '05555 666666', '5', 'NN44 5JN  ')",

    "CREATE TABLE Orders ( orderid int NOT NULL , custid int NOT NULL , productid int NOT NULL , Quantity int NOT NULL , UnitPrice FLOAT NOT NULL ) ",
    "insert into orders values(1, 5, 4, 1, 600)",
    "insert into orders values(2, 3, 1, 4, 100)",
    "insert into orders values(3, 5, 6, 5, 500)",
    "insert into orders values(4, 1, 10, 1, 85)",
    "insert into orders values(5, 1, 16, 2, 250)",
    "insert into orders values(6, 4, 9, 1, 250)",
    "insert into orders values(7, 2, 2, 2, 110)",
    "insert into orders values(8, 3, 12, 1, 180)",

    "CREATE TABLE ProductCategories ( categoryid int NOT NULL , description varchar (50) NOT NULL )",
    "insert into ProductCategories values(1, 'hard disks')",
    "insert into ProductCategories values(2, 'RAM')",
    "insert into ProductCategories values(3, 'video cards')",
    "insert into ProductCategories values(4, 'processors')",

    "CREATE TABLE Product ( productid int NOT NULL , categoryid int NOT NULL, Description varchar (50) NOT NULL , UnitPrice FLOAT NOT NULL )",
    "insert into product values(1,  1, 'HD SCSI 15GB',     100)",
    "insert into product values(2,  1, 'HD UDMA66 30GB',   110)",
    "insert into product values(3,  4, 'Athlon 850',       200)",
    "insert into product values(4,  4, 'Athlon 1100',      600)",
    "insert into product values(6,  4, 'Pentium III 750',  500)",
    "insert into product values(7,  4, 'Pentium III 850',  600)",
    "insert into product values(8,  4, 'Pentium III 1000', 750)",
    "insert into product values(9,  4, 'Celeron 600',      250)",
    "insert into product values(10, 3, '16MB Video Card',   98)",
    "insert into product values(11, 3, '32MB Video Card',  120)",
    "insert into product values(12, 3, '64MB Video Card',  180)",
    "insert into product values(13, 2, '32MB DIMM',         25)",
    "insert into product values(14, 2, '64MB DIMM',         60)",
    "insert into product values(15, 2, '128MB DIMM',       110)",
    "insert into product values(16, 2, '256MB DIMM',       250)",
    "insert into product values(17, 2, '512MB DIMM',       600)"
  };
}