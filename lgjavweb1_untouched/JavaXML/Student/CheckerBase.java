
import java.io.File;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public abstract class CheckerBase extends DefaultHandler {

  /**
   * check a single file to see if its size or modification time has changed
   * @param fileToCheck the file to be checked
   * @param size the original size of the file
   * @param modtime the original modification time
   */
   
  void checkOne(File fileToCheck, String size, String modtime) {
	  String name = fileToCheck.getPath();
	  if(!fileToCheck.exists())
	    System.err.println(name + " no longer exists");
	  else {
	    if(size != null && !("" + fileToCheck.length()).equals(size))
		    System.err.println(name + " size changed");
	    else if(modtime != null && !("" + fileToCheck.lastModified()).equals(modtime))
		    System.err.println(name + " modification time has changed");
    }
  }

  /**
   * standard SAX ErrorHandler callbacks
   */

  public void fatalError(SAXParseException e) throws SAXException {
	  throw new SAXException(e.getMessage());
  }
  
  public void error(SAXParseException e) throws SAXException {
	  throw new SAXException(e.getMessage());
  }

  public void warning(SAXParseException e) throws SAXException {
	  throw new SAXException(e.getMessage());
  }
}

