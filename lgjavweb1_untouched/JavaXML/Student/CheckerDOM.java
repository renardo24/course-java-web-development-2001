
import java.io.File;

import org.w3c.dom.*;
import org.xml.sax.*;
import org.apache.xerces.parsers.*;

/**
 * Sample solution for DOM based XML reader exercise
 */

public class CheckerDOM extends CheckerBase {

  /**
   * recursively check the contents of the given directory
   *
   * @param dirElement the DOM element representing the directory
   * @param curDir a File for the directory
   */

  void checkDirectory(Element dirElement, File curDir) {
    // your code goes here
  }

  /**
   * parse the supplied XML file and check whether the files and
   * directories that it specifies have changed
   *
   * @param XMLFile the name of the XML file to read
   */
  void checkAll(String XMLFile) throws Exception {
    // your code goes here
  }

  /**
   * Simple command line test harness
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) throws Exception {
	  // check using the file supplied on the command line
	  if(args.length > 0)
	    new CheckerDOM().checkAll(args[0]);
  }
}

