
import java.io.File;

import org.xml.sax.*;
import org.apache.xerces.parsers.*;

/**
 * Sample solution for SAX based XML reader exercise
 */

public class CheckerSAX extends CheckerBase {

  private File curDir;	// the directory current being processed

  /**
   * standard SAX callback method invoked once before the document's
   * content is processed
   */

  public void startDocument() throws SAXException {
    // your code goes here
  }

  /**
   * standard SAX callback method that is invoked when the start tag
   * for each element has been parsed
   *
   * @param namespaceURI not used
   * @param locaName not used
   * @param qName the qualified name of the element
   * @param atts the element's attributes
   */

  public void startElement(String namespaceURI, String localName,
                           String qName, Attributes atts) throws SAXException {
    // your code goes here
  }

  /**
   * standard SAX callback method that is invoked when the end tag
   * for each element has been parsed
   *
   * @param namespaceURI not used
   * @param locaName not used
   * @param qName the qualified name of the element
   */

  public void endElement(String namespaceURI, String localName,
			                   String qName) throws SAXException {
    // your code goes here
  }

  /**
   * parse the supplied XML file and check whether the files and
   * directories that it specifies have changed
   *
   * @param XMLFile the name of the XML file to read
   */

  void checkAll(String XMLFile) throws Exception {
    // your code goes here
  }

  /**
   * Simple command line test harness
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) throws Exception {
  	if(args.length > 0)
	    new CheckerSAX().checkAll(args[0]);
  }
}

