
import java.io.*;

/**
 * consumers of FileSystemWalker events implement this
 */

public interface FileSystemWalkerHandler {
    void fsStart(File rootDir);               // called at start of processing
    void fsEnd(File rootDir);                 // called at end of processing
    void dirStart(File directory, int depth); // called at start of directory
    void dirEnd(File directory, int depth);   // called at end of directory
    void file(File file, int depth);          // called for each file
}
