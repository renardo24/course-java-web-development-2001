
import java.io.*;

/**
 * write output from file system walker as XML (simply)
 */

class FileSystemWalkerSimple implements FileSystemWalkerHandler {

  PrintStream out; // the output stream

  FileSystemWalkerSimple(PrintStream out) {
    this.out = out;
  }

  /**
   * called at start of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsStart(File rootDir) {
    out.println("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
	  out.println("<!DOCTYPE root SYSTEM \"fs.dtd\">");
	  String prefixAttr = rootDir.getParent();
	  if(prefixAttr != null)
	    out.println("<root prefix=\"" + prefixAttr + "\">");
	  else
	    out.println("<root>");
  }

  /**
   * called at end of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsEnd(File rootDir) {
	  out.println("</root>");
  }

   /**
   * called at start of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirStart(File directory, int depth) {
	  indent(depth);
	  out.print("<directory name=\"" + directory.getName() + "\" ");
	  out.println("modtime=\"" + directory.lastModified() + "\">");
  }

  /**
   * called at end of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirEnd(File directory, int depth) {
	  indent(depth);
	  out.println("</directory>");
  }

  /**
   * called when processing a file
   *
   * @param file the file being processed
   * @param depth the directory nesting
   */
  public void file(File file, int depth) {
	  indent(depth);
	  out.println("<file name=\"" + file.getName() +
		       	    "\" size=\"" + file.length() +
			          "\" modtime=\"" + file.lastModified() + "\"/>");
  }

  /**
   * outputs depth*2 spaces
   *
   * @param depth the indentation level
   */
  private void indent(int depth) {
	  while(depth-- >= 0)
	    out.print("  ");
  }

  /**
   * command line utility to output file tree information as XML
   */
  public static void main(String[] args) throws Exception {
    PrintStream out = System.out;
    if(args.length > 1)
      out = new PrintStream(new FileOutputStream(args[1]));
	  if(args.length > 0)
	    new FileSystemWalker(new FileSystemWalkerSimple(out)).walk(args[0]);
    else
      System.err.println("usage: FileSystemWalkerSimple rootdir [outfile]");
  }
}
