
import java.io.*;
import org.w3c.dom.*;
import org.apache.xerces.dom.*;
import org.apache.xml.serialize.*;

/**
 * write output from file system walker as XML using DOM
 */

class FileSystemWalkerDOM implements FileSystemWalkerHandler {

  PrintStream out;       // the output stream

  FileSystemWalkerDOM(PrintStream out) {
    this.out = out;
  }

  /**
   * called at start of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsStart(File rootDir) {
    // your code goes here
  }

  /**
   * called at end of processing tree
   *
   * @param rootDir the root of the directory tree to process
   */
  public void fsEnd(File rootDir) {
    // your code goes here
  }

  /**
   * called at start of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirStart(File directory, int depth) {
    // your code goes here
  }

  /**
   * called at end of processing a directory
   *
   * @param directory the directory being processed
   * @param depth the directory nesting
   */
  public void dirEnd(File directory, int depth) {
  	// your code goes here
  }

  /**
   * called when processing a file
   *
   * @param file the file being processed
   * @param depth the directory nesting
   */
  public void file(File file, int depth) {
	  // your code goes here
  }

  /**
   * command line utility to output file tree information as XML
   */
  public static void main(String[] args) throws Exception {
    PrintStream out = System.out;
    if(args.length > 1)
      out = new PrintStream(new FileOutputStream(args[1]));
	  if(args.length > 0)
	    new FileSystemWalker(new FileSystemWalkerDOM(out)).walk(args[0]);
    else
      System.err.println("usage: FileSystemWalkerDOM rootdir [outfile]");
  }
}
