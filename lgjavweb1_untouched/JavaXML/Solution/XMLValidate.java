
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import org.apache.xerces.parsers.*;

public class XMLValidate extends DefaultHandler {

  Locator locator;

  /**
   * standard SAX ErrorHandler callbacks
   */
  public void fatalError(SAXParseException e) throws SAXException {
	  throw new SAXException(locator.getSystemId() + ":" +
                           locator.getLineNumber() + " " +
                           e.getMessage());
  }
  public void error(SAXParseException e) throws SAXException {
	  throw new SAXException(locator.getSystemId() + ":" +
                           locator.getLineNumber() + " " +
                           e.getMessage());
  }
  public void warning(SAXParseException e) throws SAXException {
	  System.err.println(locator.getSystemId() + ":" +
                       locator.getLineNumber() + " " +
                       e.getMessage());
  }

  /**
   * standard SAX ContentHandler callback
   */
  public void setDocumentLocator(Locator locator) {
    // save the supplied Locator for good error reporting
    this.locator = locator;
  }

  /**
   * command line XML file validator
   */
  public static void main(String[] args) {
	  if(args.length > 0) {
	    try {
		    try {
          // get a SAX parser
	        SAXParser p = new SAXParser();
          // create an XMLValidate
          XMLValidate v = new XMLValidate();
	        // turn on validation
	        p.setFeature("http://xml.org/sax/features/validation", true);
          // set the error handler -- this is also important because
	        // otherwise we won't get any validation error messages
	        p.setErrorHandler(v);
	        // set the content handler -- this is also important
	        // because otherwise we won't be given a Locator
	        p.setContentHandler(v);
	        // parse the supplied file
		      p.parse(args[0]);
          // if we get here the file must have been OK
          System.err.println(args[0] + " is OK");
		    }
		    catch (SAXException e) {
		      System.err.println(e.getMessage());
		    }
	    }
	    catch (Exception e) {
		    System.err.println(e);
	    }
	  }
  }
}

