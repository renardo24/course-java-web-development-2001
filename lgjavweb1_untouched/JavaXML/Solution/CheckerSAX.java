
import java.io.File;

import org.xml.sax.*;
import org.apache.xerces.parsers.*;

/**
 * Sample solution for SAX based XML reader exercise
 */

public class CheckerSAX extends CheckerBase {

  private File curDir;	// the directory current being processed

  /**
   * standard SAX callback method invoked once before the document's
   * content is processed
   */

  public void startDocument() throws SAXException {
	  // initialise the current directory
	  curDir = new File(".");
  }

  /**
   * standard SAX callback method that is invoked when the start tag
   * for each element has been parsed
   *
   * @param namespaceURI not used
   * @param locaName not used
   * @param qName the qualified name of the element
   * @param atts the element's attributes
   */

  public void startElement(String namespaceURI, String localName,
                           String qName, Attributes atts) throws SAXException {
	  if(qName.equals("root")) {
	    String prefix = atts.getValue("prefix");
	    if(prefix != null)
		    curDir = new File(prefix);
    }
	  else {
	    // construct a File for the current element
	    File curFile = new File(curDir, atts.getValue("name"));
	    // get the size and modtime (size will be null for directories)
	    String size = atts.getValue("size");
	    String modtime = atts.getValue("modtime");
	    // check this file
	    checkOne(curFile, size, modtime);
	    // if the element is a directory, make it is the current
	    // directory
	    if(qName.equals("directory"))
		    curDir = curFile;
	  }
  }

  /**
   * standard SAX callback method that is invoked when the end tag
   * for each element has been parsed
   *
   * @param namespaceURI not used
   * @param locaName not used
   * @param qName the qualified name of the element
   */

  public void endElement(String namespaceURI, String localName,
			                   String qName) throws SAXException {
  	// if the element just ending is a directory, trim the current
	  // directory back one level
	  if(qName.equals("directory"))
	    curDir = curDir.getParentFile();
  }

  /**
   * parse the supplied XML file and check whether the files and
   * directories that it specifies have changed
   *
   * @param XMLFile the name of the XML file to read
   */

  void checkAll(String XMLFile) throws Exception {
	  try {
	    // get a SAX parser
	    SAXParser p = new SAXParser();
	    // turn on validation -- this is important because we want
	    // to be sure that all the attributes have been specified
	    p.setFeature("http://xml.org/sax/features/validation", true);
	    // turn on namespace prefixes -- this guarantees that the
	    // qualified names will be delivered to the startElement
	    // and endElement methods
	    p.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
      // set the error handler -- this is also important because
	    // otherwise we won't get any validation error messages
	    p.setErrorHandler(this);
	    // set the content handler -- this is also important
	    // because otherwise we won't be notified of the documents
	    // content
	    p.setContentHandler(this);
	    // parse the supplied file
	    p.parse(XMLFile);
	  }
	  catch (SAXException e) {
	    System.err.println(e.getMessage());
	  }
  }

  /**
   * Simple command line test harness
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) throws Exception {
  	if(args.length > 0)
	    new CheckerSAX().checkAll(args[0]);
  }
}

