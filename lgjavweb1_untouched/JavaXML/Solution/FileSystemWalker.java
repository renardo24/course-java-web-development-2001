
import java.io.*;

/**
 * support class that walks over a given filesystem tree invoking
 * callback methods for each file and directory found
 */

public class FileSystemWalker {

  private FileSystemWalkerHandler handler;

  /**
   * @param handler the callback handler
   */
  FileSystemWalker(FileSystemWalkerHandler handler) {
	  this.handler = handler;
  }

  /**
   * walk the tree rooted at the given directory
   *
   * @param rootDir the name of the root directory
   */
  void walk(String rootDir) throws Exception {
	  File rd = new File(rootDir);
    // do some sanity checking
	  if(!rd.exists())
	    throw new Exception(rootDir + " does not exist");
	  if(!rd.isDirectory())
	    throw new Exception(rootDir + " is not a directory");
    // notify handler of start of filesystem
	  handler.fsStart(rd);
    // process the root directory
	  doDir(rd, 0);
    // notify handler of end of filesystem
	  handler.fsEnd(rd);
  }

  /**
   * processes the entries for a given directory
   *
   * @param dir the directory to process
   * @param depth the depth of the current directory
   */
  private void doDir(File dir, int depth) {
    // notify handler of start of directory processing
	  handler.dirStart(dir, depth);
    // obtain directory contents
	  File[] contents = dir.listFiles();
	  if(contents != null) {
      // process each entry
	    for(int i = 0; i < contents.length; ++i) {
		    if(contents[i].isDirectory()) {
          // recursively handle sub-directories
		      doDir(contents[i], depth + 1);
        }
		    else {
          // notify handler about this file
		      handler.file(contents[i], depth + 1);
        }
	    }
	  }
    // notify handler of end of directory processing
	  handler.dirEnd(dir, depth);
  }
}

