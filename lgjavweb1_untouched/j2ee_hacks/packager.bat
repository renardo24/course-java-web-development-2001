@echo off
rem 
rem Set JAVA_HOME and J2EE_HOME before running this script.
rem

if not "%J2EE_HOME%" == "" goto CONT0
echo ERROR: Set J2EE_HOME before running this script.
goto END
:CONT0

if EXIST "%J2EE_HOME%\bin\setenv.bat" goto CONT1
echo ERROR: Set J2EE_HOME to the path of a valid j2sdkee.
goto END
:CONT1

call %J2EE_HOME%\bin\setenv.bat

if not "%JAVA_HOME%" == "" goto CONT2
echo ERROR: Set JAVA_HOME before running this script.
goto END
:CONT2

if EXIST "%JAVA_HOME%\bin\java.exe" goto CONT3
echo ERROR: Set JAVA_HOME to the path of a valid jdk.
goto END
:CONT3

@set ARG1=%1
@shift
@set ARG2=%1
@shift
@set ARG3=%1
@shift
@set ARG4=%1
@shift
@set ARG5=%1
@shift

rem @echo on
%JAVACMD% -classpath "%CPATH%" "-Dcom.sun.enterprise.home=%J2EE_HOME%" com.sun.enterprise.tools.packager.Main %ARG1% %ARG2% %ARG3% %ARG4% %ARG5% %1 %2 %3 %4 %5 %6 %7 %8

:END
