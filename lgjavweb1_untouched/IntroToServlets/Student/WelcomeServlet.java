import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

// exercise one step one
// ensure that the servlet class extends HttpServlet
// declare an instance variable called "out" of type PrintWriter

public class WelcomeServlet 
{
  


// exercise one step two
// override doGet 
// have doGet call doPost passing the appropriate parameters
// to minimize the amount of code required to process both types of request

// override doPost

  { // start of doPost






// exercise one step three
// set the content type for the response to text/html
// call the response object's getWriter() method
// store a reference to the writer object in the out variable declared earlier







// exercise one step four
// connect to see if a value was passed for the parameter, p2 in the request
// if no value was passed display the welcome page by calling doIntroPage
// else display the results page by calling doResultsPage







  } // end of doPost
 




  public void doIntroPage(HttpServletRequest req, HttpServletResponse rsp)
  {
    doIntroHeaders(req, rsp);
    doIntroBody(req, rsp);
    doFooter(req, rsp);
  }

  public void doResultsPage(HttpServletRequest req, HttpServletResponse rsp)
  {
    doResultsHeader(req, rsp);
    doResultsBody(req, rsp);
    doFooter(req, rsp);
  }





  public void doIntroHeaders(HttpServletRequest req, HttpServletResponse rsp)
  {

// exercise one step five
// add code to begin building the response to be sent to the client
// build the HTML from the opening <HTML> to the closing </HEAD> tag
// follow the instructions in the exercise notes for details on the page title









  }
  
  public void doIntroBody(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise one step six
// add code which begins the body of the HTML page
// include a prompt which asks the user to submit their details
// then call doForm(...)







  }

  public void doFooter(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise one step seven
// add a footer to your HTML page
// include code informing the user of the time their request was processed
// Note: ensure you close all tags remaining open (i.e. don't forget </BODY> and </HTML>, etc.)






  }

  public void doResultsHeader(HttpServletRequest req, HttpServletResponse rsp)
  {
// exercise one step eight
// add code to begin building the results page to be sent to the client
// if a value exists for the orgName initialization parameter, 
// use it in the title of the page else the title should be "Results Page"














  }

  public void doResultsBody(HttpServletRequest req, HttpServletResponse rsp)
  {
// Exercise one step nine
// build the body for the results page
// thank the user by name for the information they submitted
// echo the information they submitted to the page
// allow the user to resubmit their information by including a call to doForm









  }


  // code for doForm provided for you

  public void doForm(HttpServletRequest req, HttpServletResponse rsp)
  {
    out.println("<form method=POST action=\"Welcome\">");
    out.println("<p>");
    out.println("Diagnostics Displayed: <input type=checkbox name=diagnostics value=ON></p>");
    out.println("<hr>");
    out.println("<p>Name:<br>");
    out.println("<input type=text name=\"name\" size=30></p>");
    out.println("Organisation:<br>");
    out.println("<input type=text name=organisation size=30></p>");
    out.println("<p>Job Title:<br>");
    out.println("<input type=text name=jobtitle size=30></p>");
    out.println("<input type=hidden name=p2></p>");
    out.println("<p><input type=submit value=Submit><input type=reset value=Reset></p>");
    out.println("</form>");
  }
}
