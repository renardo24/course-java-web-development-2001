
import java.io.Serializable;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;

// exercise four step one
// have the DealsBean class implements the Serializable interface
// and provide a default no-argument constructor


public class DealsBean
{

// exercise four step two
// implement a read/write JavaBean property called interest
// of type String

// exercise four step three
// implement a write-only JavaBean property called default
// of type String

// exercise four step four
// declare a read only JavaBean property called deals to return current deals
// use the interest property to determine which deals should be returned

// the deals data should be fetched from the database whose JNDI identifier
// is "jdbc/ServletDB" and suitably formated with HTML i.e. as a simple table

// The SQL we give here will select the description and the unit price of those products
// whose category description matches the string in the variable called interest.
// The result of executing this query should be a table containing two columns:
// product description (a String) and unit price (an int)

/*
      String sql = "SELECT Product.Description, Product.UnitPrice " +
        " FROM Product INNER JOIN " +
        " ProductCategories ON Product.categoryid = ProductCategories.categoryid" +
        " WHERE ProductCategories.description = '" + interest + "'";
*/

// If no interest is specified, return the value of the default property

}
