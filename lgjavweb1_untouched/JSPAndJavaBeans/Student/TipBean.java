

// exercise one step one
// provide conformance with good JavaBean development practices
// by ensuring your bean implements the Serializable interface
// and provides a default no argument constructor

public class TipBean
{

// exercise one step two
// replace the sample tips or extend them with tips of your own

  private String [] tips = { 
  				"Don't touch wires to see if they're live!",
				"Adding 'throws Exception' to a <B>main</B> method is not 'error handling'.",
				"JSPs really are this simple.",
				"Take a coffee break if you've got this to work.",
				"If all else fails, compile from the command line.",
				"This would be tougher to write in a servlet.",
				"Never order a curd tart when drunk."
			   };

								
// exercise one step three
// define a read only property called RandomTip to return a random tip of the day
   

}