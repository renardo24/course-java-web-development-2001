
import java.io.Serializable;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;

public class DealsBean implements Serializable {

  public DealsBean() {
  }

  private String interest;
  private String _default = "";

  public void setInterest(String newInterest) {
    interest = newInterest;
  }

  public String getInterest() {
    return interest;
  }

  public void setDefault(String newDefault) {
    _default = newDefault;
  }

  public String getDeals() {

    if(interest == null)
      return _default;

    try {
      // get the initial context
      Context ic = new InitialContext();
      // now get a DataSource for the desired database
      DataSource ds = (DataSource)ic.lookup("jdbc/ServletDB");
      // now get a connection to that database
      Connection conn = ds.getConnection();
      // get a Statement
      Statement stmt = conn.createStatement();
      // build the SQL query
      String sql = "SELECT Product.Description, Product.UnitPrice " +
        " FROM Product INNER JOIN " +
        " ProductCategories ON Product.categoryid = ProductCategories.categoryid" +
        " WHERE ProductCategories.description = '" + interest + "'";
      // execute the query
      ResultSet rs = stmt.executeQuery(sql);

      // iterate through the rows returned by the query building up the result
      String result = "";
      while(rs.next()) {
        // the product description is a String
        // the unit price is an integer
        result += "<tr><td>" + rs.getString(1) + "<td>�" + rs.getInt(2) + "\n";
      }
      // close the connection to avoid leaks
      conn.close();
      // return the result
      if(result.equals(""))
        return "Sorry, please check another day for " + interest + " deals";
      else
        return "<table>" + result + "</table>";
    }
    catch(Exception e) {
      // output detail to stderr
      e.printStackTrace(System.err);
      // return diagnostic message as result
      return "<B>" + e.getMessage() +"</B>";
    }
  }
}