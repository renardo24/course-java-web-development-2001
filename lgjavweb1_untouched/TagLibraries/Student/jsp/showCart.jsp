<html>
<HEAD>
<TITLE>Cart Contents</TITLE>
<%@ page session="true"%>
<%@ page import="java.util.*" %>
<%@ page import="cart.*" %>
<jsp:useBean id='cart' scope='session' class='cart.ShoppingCart' />
</HEAD>

<BODY BGCOLOR="black" TEXT= "white">

<FONT SIZE=4>
<UL>
<LI>
Cart belongs to : <jsp:getProperty name="cart" property="cartOwner" />
<BR>
<BR>
<LI>Session Duration :
<%
  long startTime= session.getCreationTime();
  long  now = new Date().getTime();
  now -= startTime;
  out.print(now + " milliseconds");
 %>
</UL>
</FONT>

<TABLE ALIGN="center" WIDTH="100", CELLSPACING="4" CELLPADDING="4">

<%
  Enumeration enum = cart.getCartItems();
  while(enum.hasMoreElements())
  {
    Cartable item = (Cartable) enum.nextElement();
%>
  	<TR>
      <TD><%= item.getCode() %></TD>
      <TD><%= item.getDescription() %></TD>
      <TD><%= item.getQuantity() %></TD>
      <TD><%= item.getCost() %></TD>
    </TR>
<%
  } // ends while
%>

</TABLE>

<BR>
<HR>
<BR>


</BODY>
</HTML>
