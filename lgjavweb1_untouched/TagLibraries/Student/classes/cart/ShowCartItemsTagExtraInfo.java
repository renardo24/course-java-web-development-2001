package cart;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.util.*;


public class ShowCartItemsTagExtraInfo extends TagExtraInfo{

    public VariableInfo[] getVariableInfo(TagData data) {
        System.out.println("ShowCartItemsTagExtraInfo:  getVariableInfo ");

        return new VariableInfo[]
            {
                new VariableInfo("code",
                                 "String",
                                 true,      // true = new variable
                                 VariableInfo.NESTED),
                new VariableInfo("description",
                                 "String",
                                 true,
                                 VariableInfo.NESTED),
                new VariableInfo("quantity",
                                 "Integer",
                                 true,
                                 VariableInfo.NESTED),
                new VariableInfo("cost",
                                 "Double",
                                 true,
                                 VariableInfo.NESTED)
            } ;
       }
}