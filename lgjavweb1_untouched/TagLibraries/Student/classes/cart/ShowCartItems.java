package cart;

import javax.servlet.jsp.*;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.*;
import java.util.*;
import java.io.*;

public class ShowCartItems extends BodyTagSupport{

  private Enumeration enum;

  public int doStartTag() throws JspException{
    return BodyTag.EVAL_BODY_TAG;
  }

  public void doInitBody(){
  // QA ToDo:
  // Recover the next Cartable element from the enumeration,
  // extract the values for the properties
  // code, description, quantity and cost
  // and put these values into the pageContext object
  // using the setAttribute command.
  }



  public int doAfterBody() throws JspException{
  // QA ToDo:
  // If the enumeration holds more Cartable items
  // recover the next Cartable element from the enumeration,
  // extract the values for the properties
  // code, description, quantity and cost
  // and put these values into the pageContext object
  // using the setAttribute command.
  // Return BodyTag.EVAL_BODY_TAG.

  // QA ToDo:
  // Alternatively, if there are no more Cartable items
  // write out the bodyContent to the enclosing JspWriter.
      return Tag.SKIP_BODY;

  }


} 