package cart;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class AddToCartServlet extends HttpServlet {

  //Process the HTTP Post request
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    // get session (or create new session if one does not already exist)
    HttpSession session = request.getSession(true);

    // Retrieve cart from session
    Object sessObject = session.getAttribute("cart");
    ShoppingCart cart = null;
    if(sessObject instanceof ShoppingCart)
      cart = (ShoppingCart) sessObject;
    // if cart is not found, create cart and add to session
    if(cart == null)
    {
      cart = new ShoppingCart();
      // find name of cart owner
      String name = (String) session.getAttribute("name");
      if(name == null)
      {
        name = request.getParameter("name");
        if(name == null) name = session.getId();
        session.setAttribute("name",name);
      }
      cart.setCartOwner(name);
      session.setAttribute("cart",cart);
    }

    // get details of item to be added to cart
    // then create Cartable item and add it to cart
    String code = "";
    String description = null;
    int quantity = 0;
    double unitCost = 5.25;


    for(int n=1;true;n++)
    {
      code = request.getParameter("code"+n);
      if (code==null) break;
      try { quantity = Integer.parseInt(request.getParameter("quantity" + n)); } catch (Exception e) { e.printStackTrace(); }
      description = request.getParameter("description" + n);
      if(description == null) description = "unknown";
      cart.addCartItem(new CartItem(code,description,quantity,unitCost*quantity));
    }

    getServletConfig().getServletContext().getRequestDispatcher("/jsp/showCart.jsp").forward(request, response);


  }

  //Get Servlet information
  public String getServletInfo() {
    return "cart.AddToCartServlet which adds itmes to a shopping cart, then forwards request to showCart.jsp";
  }


  //Initialize global variables
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doPost(request, response);
  }
} 