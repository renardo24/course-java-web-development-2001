package cart;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.util.*;

public class SetUpScriptingVariablesTagExtraInfo extends TagExtraInfo{

    public VariableInfo[] getVariableInfo(TagData data) {
        System.out.println("SetUpScriptingVariableTagExtraInfo:  getVariableInfo ");
        Enumeration enum =   data.getAttributes();
        while (enum.hasMoreElements())
        {
          String key = (String)enum.nextElement();
          String val = data.getAttribute(key).toString();
          System.out.println("Key = " + key + " value = " + val);
        }
         return new VariableInfo[]
            {
                new VariableInfo(data.getAttributeString("id"),
                                 "cart.ShoppingCart",
                                 true,      // true = new variable
                                 VariableInfo.AT_BEGIN)
            };
    }
}
