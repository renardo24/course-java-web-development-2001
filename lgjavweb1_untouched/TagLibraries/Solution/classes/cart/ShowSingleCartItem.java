
//Title:        Shopping Cart Web Application
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       
//Company:      QA Training
//Description:  Classes for use with an online toy shop

package cart;


import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.util.*;
import java.io.*;
import utils.*;

public class ShowSingleCartItem extends BodyTagSupport{

private Cartable cartItem;
private Iterate parent;

  public ShowSingleCartItem() {
  }

  public int doStartTag(){
    System.out.print("ShowSingleCartItem: in doStartTag()  ..");
    parent = (Iterate) TagSupport.findAncestorWithClass(this, Iterate.class);
    Object obj = parent.getItem();
    if(obj instanceof Cartable)
    {
      System.out.print("found Cartable " + obj);    
      cartItem = (Cartable) obj;
      setCartItemData();
      return BodyTag.EVAL_BODY_TAG;
    }
    else
    {
       try{
        pageContext.getOut().print("Couldn't find parent iterator item");
       }catch(IOException io){
          io.printStackTrace();
       }
       return Tag.SKIP_BODY;
    }
  }

  private void setCartItemData(){
      pageContext.setAttribute("code",cartItem.getCode());
      pageContext.setAttribute("description",cartItem.getDescription());
      pageContext.setAttribute("quantity",new Integer(cartItem.getQuantity()));
      pageContext.setAttribute("cost",new Double(cartItem.getCost()));
  }

  public int doAfterBody() throws JspException{
    System.out.println("ShowSingleCartItem: in doAfterBody()");
    try{
        bodyContent.writeOut(bodyContent.getEnclosingWriter());
    }catch(IOException io){
          io.printStackTrace();
    }
      return Tag.SKIP_BODY;
    }
} 