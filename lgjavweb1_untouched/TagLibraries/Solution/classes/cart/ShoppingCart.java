
//Title:        Shopping Cart Web Application
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       
//Company:      QA Training
//Description:  Classes for use with an online toy shop

package cart;

import java.util.*;

public class ShoppingCart {
  private String cartOwner = "unknown";
  private Vector items = new Vector();
  private double totalCost = 0.0;

  public ShoppingCart() {
  }

  public String getCartOwner() {
    return cartOwner;
  }

  public void setCartOwner(String newCartOwner) {
    cartOwner = newCartOwner;
  }

  public Enumeration getCartItems(){
    return items.elements();
  }

  public void addCartItem(Cartable newItem){
    totalCost += newItem.getCost();
    items.add(newItem);
  }

  public boolean removeCartItem(Cartable removeItem){
    if( items.removeElement(removeItem))
    {
      totalCost -= removeItem.getCost();
      return true;
    }
    return false;
  }

  public double getTotalCost(){
    return totalCost;
  }

}