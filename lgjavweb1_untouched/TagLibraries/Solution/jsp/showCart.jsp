<html>
<HEAD>
<TITLE>Cart Contents</TITLE>
<%@ page session="true"%>
<%@ taglib uri="http://www.qatraining.com/jsp/qaTagLibrary_1" prefix="qa" %>
<jsp:useBean id='cart' scope='session' class='cart.ShoppingCart' />
</HEAD>

<BODY BGCOLOR="red" TEXT= "white">

<FONT SIZE=4>
<UL>
<LI>
Cart belongs to : <jsp:getProperty name="cart" property="cartOwner" />
<BR>
<BR>
<LI>Session Duration : <qa:GetElapsedTime startTime="<%= session.getCreationTime() %>" />
</UL>
</FONT>

<TABLE ALIGN="center" WIDTH="100", CELLSPACING="4" CELLPADDING="4">

<qa:ShowCartItems cartHoldingItems="<%= cart %>">
	<TR>
    <TD><%= code %></TD>
    <TD><%= description %></TD>
    <TD><%= quantity %></TD>
    <TD><%= cost %></TD>
  </TR>
</qa:ShowCartItems>

</TABLE>

<BR>
<HR>
<BR>

<!-- Setting Up Scripting Variable Named "cart2" -->

<qa:SetUpScriptingVariables id="cart2" />

<!-- could now replace <%= cart.getCartItems() %> in the code
     below with <%= cart2.getCartItems() %>
-->

<H2>Trying Alternative Version Using Iterate Tag</H2>
<BR>

<TABLE ALIGN="center" WIDTH="100", CELLSPACING="4" CELLPADDING="4">

<qa:Iterate enumeration="<%= cart.getCartItems() %>" >
	<qa:ShowSingleCartItem>
  <TR>
    <TD><%= code %></TD>
    <TD><%= description %></TD>
    <TD><%= quantity %></TD>
    <TD><%= cost %></TD>
  </TR>
	</qa:ShowSingleCartItem>
</qa:Iterate>

</TABLE>

</BODY>
</HTML>
