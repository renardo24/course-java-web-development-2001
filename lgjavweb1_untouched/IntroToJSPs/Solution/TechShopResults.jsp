<HTML>

<head>
<title></title>
</head>

<body bgcolor="#FFFF00">

<p align="center"><font face="Comic Sans MS" size="7" color="#FF0000">Welcome to</font></p>
<p align="center"><img src=image001.gif width="194" height="76"></p>
<p><font face="Comic Sans MS">Our full <a href="catalogue.html">online catalogue</a> contains hundreds of new deals.</font></P>
<P><font face="Comic Sans MS">We have several deals relating to
<%=request.getParameter("interest")==null?"technology":request.getParameter("interest")%>
 right now. Please check them out below:</font></p>
<%=getDeals(request.getParameter("interest"))%>


<p>The stores nearest to you are listed below (or all stores if we're not sure where you are!):</p>
<%=getStores(request.getParameter("area"))%>
<HR>
Please feel free to call in for more excellent bargains.
<HR>
Processed at <%=new java.util.Date()%> by <%=request.getServerName() + ":" + request.getServerPort()%>
</body>
<%!
  // declare an array of Outlet Details
  private String [] addresses =
    {
  	 "<P>1 High Street<BR> <a href=\"mailto:cardiff.admin@techshop.com\">Cardiff</a><BR>CF1  1QA<BR></P>" +
  	 "<P>3 Castle Street<BR> <a href=\"mailto:swansea.admin@techshop.com\">Swansea</a><BR>SA1  5QA<BR></P>",

  	 "<P>5 Main Street<BR> <a href=\"mailto:belfast.admin@techshop.com\">Belfast</a><BR>BE2  1QA<BR></P>",

  	 "<P>7 Long Street<BR> <a href=\"mailto:glasgow.admin@techshop.com\">Glasgow</a><BR>GW4  9QA<BR></P>" +
  	 "<P>17 St Michael Road<BR> <a href=\"mailto:edinburgh.admin@techshop.com\">Edinburgh</a><BR>ED4  1QA<BR></P>",

  	 "<P>4 London Road<BR> <a href=\"mailto:oxford.admin@techshop.com\">Oxford</a><BR>OX15  7QA<BR></P>" +
  	 "<P>23 London Road<BR> <a href=\"mailto:birmingham.admin@techshop.com\">Birmingham</a><BR>BR54  9QA<BR></P>" +
  	 "<P>15 Liverpool Street<BR> <a href=\"mailto:london.admin@techshop.com\">London</a><BR>W1  5QA<BR></P>",

  	 "<P>100 Lower Baggott Street<BR> <a href=\"mailto:dublin.admin@techshop.com\">Dublin</a><BR>DB1  1QA<BR></P>",

  	 "<P>1 Rue des Champes<BR> <a href=\"mailto:Nantes.admin@techshop.com\">Nantes</a><BR>France<BR></P>"
    };
  									
   // declare a method to return store details given an area
 
   public String getStores(String locale)
   {
     if("wales".equalsIgnoreCase(locale))
     {
     	return addresses[0];
     }
     if("ni".equalsIgnoreCase(locale))
     {
      	return addresses[1];
     }
     if("scotland".equalsIgnoreCase(locale))
     {
      	return addresses[2];
     }
     if("england".equalsIgnoreCase(locale))
     {
      	return addresses[3];
     }
     if("ireland".equalsIgnoreCase(locale))
     {
      	return addresses[4];
     }
     if("france".equalsIgnoreCase(locale))
     {
      	return addresses[5];
     }
     
     
     String adds =  "<H3>All addresses</H3>";
     
     for(int i=0;i<addresses.length;i++)
     {
       adds+=addresses[i];
     }
     return adds;
   }

   private String [] deals = 
     {
// General Technology
       "<A href=underconstruction.html>Full System - Celeron 600MHz</A> - �975<BR>" +
       "<A href=underconstruction.html>Full System - PIII 800MHz</A> - �1225<BR>" +
       "<A href=underconstruction.html>Full System - Athlon 850MHz</A> - �1450<BR>" +
       "<A href=underconstruction.html>Full System - Athlon 1100MHz</A> - �1950<HR>",
// Hard Disks
       "<A href=underconstruction.html>15GB hard drive</A> - �100<BR>" +
       "<A href=underconstruction.html>30GB hard drive</A> - �160<BR>" +
       "<A href=underconstruction.html>50GB hard drive</A> - �220<BR>" +
       "<A href=underconstruction.html>75GB hard drive</A> - �350<HR>",
// RAM
       "<A href=underconstruction.html>64MB PC100 DIMM</A> - �50<BR>" +
       "<A href=underconstruction.html>128MB PC100 DIMM</A> - �100<BR>" +
       "<A href=underconstruction.html>256MB PC100 DIMM</A> - �220<BR>" +
       "<A href=underconstruction.html>512MB PC100 DIMM</A> - �520<HR>",
// Video Cards
       "<A href=underconstruction.html>16MB Video Card</A> - �70<BR>" +
       "<A href=underconstruction.html>32MB Video Card</A> - �110<BR>" +
       "<A href=underconstruction.html>64MB Video Card</A> - �220<HR>",
// processors
       "<A href=underconstruction.html>700MHz Athlon</A> - �150<BR>" +
       "<A href=underconstruction.html>800MHz Athlon</A> - �200<BR>" +
       "<A href=underconstruction.html>850MHz Athlon</A> - �250<BR>" +
       "<A href=underconstruction.html>950MHz Athlon</A> - �450<BR>" +
       "<A href=underconstruction.html>1100MHz Athlon</A> - �650<HR>"
     };

   public String getDeals(String interest) {
     
     if("hard disks".equalsIgnoreCase(interest))
     {
       return deals[1];
     }
     if("RAM".equalsIgnoreCase(interest))
     {
       return deals[2];
     }
     if("video cards".equalsIgnoreCase(interest))
     {
       return deals[3];
     }
     if("processors".equalsIgnoreCase(interest))
     {
       return deals[4];
     }
     if("technology".equalsIgnoreCase(interest))
     {
       return deals[0];
     }

     return "Please tell us what you are currently looking for on our <A href=\"Welcome\">welcome page</A>";
   }

%>
</HTML>



